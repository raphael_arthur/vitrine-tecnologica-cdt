import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs/Rx';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { utf8Encode } from '@angular/compiler/src/util';
import { Form } from '@angular/forms/src/directives/form_interface';

@Injectable()

export class FileUploadService{

    constructor(public http:Http){
    }

    public PostFile(body:FormData,url:string):Observable<any>{
        let headers = new Headers({ 'Content-Type': '*' });
        let options = new RequestOptions({ headers: headers });
        return this.http
        .post(url,body);
    }

    public GetImage(url:string,body:string):Observable<any>{
        let realBody = new FormData();
        realBody.append('idinventor',body);
        return this.http
        .post(url,realBody);        
    }

}