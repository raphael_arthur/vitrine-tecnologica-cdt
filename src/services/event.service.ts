import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';
import { ServerService } from '../app/core/services/server.service';
@Injectable()
export class EventService extends ServerService {

    public events_found:any;

    constructor(http:Http){
        super(http);
    }

    createEvent(formData:string):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'event_name=' + formData['event_name'] + '&begin_event=' + formData['begin_date'] + '&end_event=' + 
                formData['end_date'] + '&adress=' + formData['adress'] + '&place=' + formData['place'] + '&event_description=' + formData['event_description'] + '&usr_id=' + sessionStorage.getItem('userID');
            this.sendPostRequest(serverData, "server_vitrine/request/event_create.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
        
    }

    getEvents(user_id:string):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'user_id=' + user_id;
            this.sendPostRequest(serverData, "server_vitrine/request/event_get.php").subscribe( (data:JSON) => {
                console.log(data);
                this.events_found = JSON.parse(data['_body']);
                console.log(this.events_found);
                observer.next(true);
            });            
        });        
    }

    getAllEvents():Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            this.sendPostRequest(serverData, "server_vitrine/request/event_getAll.php").subscribe( (data:JSON) => {
                console.log(data);
                this.events_found = JSON.parse(data['_body']);
                console.log(this.events_found);
                observer.next(true);
            });
            
        });
        
    }

    editEvent(event:any):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'event_name=' + event.nome + '&begin_event=' + event.inicio + '&end_event=' + 
                event.fim + '&adress=' + event.endereco + '&place=' + event.local + '&event_description=' + 
                event.descricao + '&usr_id=' + event.usuario_idusuario + '&id=' + event.idevento;
            this.sendPostRequest(serverData, "server_vitrine/request/event_edit.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    delEvent(id_event:any):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'id=' + id_event;
            this.sendPostRequest(serverData, "server_vitrine/request/event_del.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    registerEvent(formData:string, event_id:string):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'event_id=' + event_id + '&firstName=' + formData['firstName'] + '&lastName=' + 
                formData['lastName'] + '&cpf=' + formData['cpf'] + '&matricula=' + formData['matricula'] + '&birthday=' + formData['birthday'] + '&email=' + formData['email'];
            this.sendPostRequest(serverData, "server_vitrine/request/event_register.php").subscribe( (data:any) => {
                console.log(data);
            });
        });
    }
}
