import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';
import { ServerService } from '../app/core/services/server.service';
@Injectable()
export class CategoryService extends ServerService {

    public category_found:any;

    constructor(http:Http){
        super(http);
    }

    getAllCategories():Observable<any>
    {
        console.log("estou dentro get ionic");
        return Observable.create( (observer:Observer<any>) => { 
            let serverData:string = "";
            this.sendPostRequest(serverData, "server_vitrine/request/category_getAll.php").subscribe( (data:JSON) => {
                console.log(data['_body']);
                this.category_found = JSON.parse(data['_body']);
                console.log(this.category_found);
                observer.next(true);
            });
            
        });
        
    }

    del(id:any):Observable<any>
    {
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'id=' + id;
            this.sendPostRequest(serverData, "server_vitrine/request/category_del.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    edit(category:any):Observable<any>
    {
        console.log(category);
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + category.nome + '&id=' + category.idcategoria;
            this.sendPostRequest(serverData, "server_vitrine/request/category_edit.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    create(category:any):Observable<any>
    {
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + category;
            this.sendPostRequest(serverData, "server_vitrine/request/category_create.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
        
    }
}