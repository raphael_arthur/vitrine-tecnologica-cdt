import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';

import { ServerService } from '../app/core/services/server.service';



@Injectable()
export class TechService extends ServerService {

    public tech_found:any;
    public tech_found_by_dep:any;
    public tec_found_by_category:any;
    public tec_found_by_inventor:any;
    public tec_found_by_key:any;
    public has_dep:any;
    public has_inventor:any;
    public has_key:any;
    public has_lab:any;
    public last_tech_id:string;
    
    constructor(http:Http){
        super(http);
    }

    getAllTechs():Observable<any>
    {
        console.log("estou dentro getEvents ionic");
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            //console.log("serverDATA: " + serverData);
            this.sendPostRequest(serverData, "server_vitrine/request/tech_getAll.php").subscribe( (data:JSON) => {
                console.log(data);
                this.tech_found = JSON.parse(data['_body']);
                console.log(this.tech_found);
                observer.next(true);
            });
            
        });
        
    }

    del(id:any):Observable<any>
    {
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'id=' + id;
            //console.log("serverDATA: " + serverData);
            this.sendPostRequest(serverData, "server_vitrine/request/tech_del.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    edit(tech:any, oldDepartment:string, newDep:string, oldinventor:string, newInventor:string, oldKey:string, newKey:string, oldUser:string, oldLab:string, newLab:string):Observable<any>
    {
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + tech.nome + "&desc=" + tech.descricao + "&dados=" + tech.dados_protecao + "&idinventor=" + newInventor 
            + "&iddepartamento=" + newDep + "&idpalavra_chave=" + newKey + "&idtech=" + tech.idtecnologia + "&idusuario=" 
            + sessionStorage.getItem('userID') + "&oldDepartment=" + oldDepartment + "&oldinventor=" + oldinventor + "&oldKey=" + oldKey + "&oldUser=" +
            oldUser + "&idLab=" + newLab + "&oldLab=" + oldLab;

            //console.log("serverDATA: " + serverData);
            this.sendPostRequest(serverData, "server_vitrine/request/tech_edit.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    create(tech:any):Observable<any>
    {
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + tech.nome + "&desc=" + tech.desc + "&dados=" + tech.dados_protecao + "&idinventor=" + tech.idinventor 
            + "&iddepartamento=" + tech.iddepartamento + "&idpalavra_chave=" + tech.idpalavra_chave + "&idlab=" + tech.idlab +"&apelido=" + tech.apelido + "&situacao=" + tech.situacao + "&resumo=" + tech.resumo + "&vantagem=" + tech.vantagem + "&licenciada=" + tech.licenciada + "&idusuario=" + sessionStorage.getItem('userID') 
            +'&fileName='+tech.fileName;

            this.sendPostRequest(serverData, "server_vitrine/request/tech_create.php").subscribe( (data:any) => {
                console.log(data['_body']);
            });
            
        });
        
    }

    getTec_has_dep(id:string):Observable<any>
    {
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "id=" + id;
            //console.log("serverDATA: " + serverData);
            this.sendPostRequest(serverData, "server_vitrine/request/tech_getTec_has_dep.php").subscribe( (data:JSON) => {
                console.log(data);
                this.has_dep = JSON.parse(data['_body']);
                console.log(this.has_dep.departamento_iddepartamento);
                observer.next(true);
            });
        });
    }

    getTec_has_lab(id:string):Observable<any>
    {
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "id=" + id;
            //console.log("serverDATA: " + serverData);
            this.sendPostRequest(serverData, "server_vitrine/request/tech_getTec_has_lab.php").subscribe( (data:JSON) => {
                console.log(data);
                this.has_lab = JSON.parse(data['_body']);
                console.log(this.has_lab.laboratorio_idlaboratorio);//id_laboratorio possivelmente sera um array
                console.log("Deve printar PRIMEIRO 111111111111111111111111");
                observer.next(true);
            });
        });
    }

    getTec_has_inventor(id:string):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "id=" + id;
            this.sendPostRequest(serverData, "server_vitrine/request/tech_getTec_has_inventor.php").subscribe( (data:JSON) => {
                console.log(data);
                this.has_inventor = JSON.parse(data['_body']);//id_inventor possivelmente sera um array
                observer.next(true);
            });            
        });        
    }

    getTec_has_key(id:string):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "id=" + id;
            this.sendPostRequest(serverData, "server_vitrine/request/tech_getTec_has_key.php").subscribe( (data:JSON) => {
                console.log(data);
                
                this.has_key = JSON.parse(data['_body']);
                console.log(this.has_key);
                observer.next(true);
            });            
        });        
    }

    search_by_category(id:string):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "id=" + id;
            this.sendPostRequest(serverData, "server_vitrine/request/tech_search_category.php").subscribe( (data:JSON) => {
                console.log(data);
                if(data['_body'] != "")
                    this.tec_found_by_category = JSON.parse(data['_body']);
                console.log(this.tec_found_by_category);
                observer.next(true);
            });            
        });
    }

    search_by_inventor():Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string   = "";
            this.sendPostRequest(serverData, "server_vitrine/request/tech_search_inventor.php").subscribe( (data:JSON) => {
                console.log(data);
                this.tech_found = JSON.parse(data['_body']);
                console.log(this.tech_found);
                observer.next(true);
            });
            
        });
    }

    search_by_key():Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            this.sendPostRequest(serverData, "server_vitrine/request/tech_search_key.php").subscribe( (data:JSON) => {
                console.log(data);
                this.tech_found = JSON.parse(data['_body']);
                console.log(this.tech_found);
                observer.next(true);
            });            
        });
    }

    search_by_department():Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            this.sendPostRequest(serverData, "server_vitrine/request/tech_search_department.php").subscribe( (data:JSON) => {
                console.log(data);
                this.tech_found = JSON.parse(data['_body']);
                console.log(this.tech_found);
                observer.next(true);
            });
            
        });
    }

    /*
    Guilherme natal: 14/03/2018
    A função BindFileToTech manda uma requisição para o php
    que faz a inserção na tabela de tecnologias e arquivos.
    Neste caso envia-se o id da tecnologia e o caminho para a
    pasta em que o arquivo foi salvo.
    */

    public BindFileToTech(data:any):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            this.sendPostRequest(serverData, "server_vitrine/request/tech_search_department.php").subscribe( (data:JSON) => {
                console.log(data);
                this.tech_found = JSON.parse(data['_body']);
                console.log(this.tech_found);
                observer.next(true);
            });            
        });   
    }

}