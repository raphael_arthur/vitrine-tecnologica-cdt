import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';
import { ServerService } from '../app/core/services/server.service';

@Injectable()

export class KeyService extends ServerService {

    public key_found:any;

    constructor(http:Http){
        super(http);
    }

    getAllKeys():Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            this.sendPostRequest(serverData, "server_vitrine/request/key_getAll.php").subscribe( (data:JSON) => {
                console.log(data);
                this.key_found = JSON.parse(data['_body']);
                console.log(this.key_found);
                observer.next(true);
            });            
        });        
    }

    del(id:any):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'id=' + id;
            //console.log("serverDATA: " + serverData);
            this.sendPostRequest(serverData, "server_vitrine/request/key_del.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    edit(key:any):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + key.nome + '&id=' + key.idpalavra_chave + "&fk=" + key.idcategoria;
            this.sendPostRequest(serverData, "server_vitrine/request/key_edit.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    create(key:string,idCat:string):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + key + "&idCat=" + idCat;
            this.sendPostRequest(serverData, "server_vitrine/request/key_create.php").subscribe( (data:any) => {
                console.log(data);
            });
        });        
    }

}