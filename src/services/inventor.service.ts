import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';
import { ServerService } from '../app/core/services/server.service';

@Injectable()

export class InventorService extends ServerService {

    public inventor_found:any;

    constructor(http:Http){
        super(http);
    }

    getAllInventores():Observable<any>
    {
        console.log("estou dentro getEvents ionic");
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            this.sendPostRequest(serverData, "server_vitrine/request/inventor_getAll.php").subscribe( (data:JSON) => {
                console.log(data);
                this.inventor_found = JSON.parse(data['_body']);
                console.log(this.inventor_found);
                observer.next(true);
            });            
        });        
    }

    del(id:any):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'id=' + id;
            this.sendPostRequest(serverData, "server_vitrine/request/inventor_del.php").subscribe( (data:any) => {
                console.log(data);
            });            
        });
    }

    edit(inventor:any):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + inventor.nome + '&id=' + inventor.idinventor + "&lattes=" + inventor.lattes;;
            this.sendPostRequest(serverData, "server_vitrine/request/inventor_edit.php").subscribe( (data:any) => {
                console.log(data);
            });            
        });
    }

    create(inventor:any):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + inventor.nome + "&lattes=" + inventor.lattes + '&fileName='+inventor.fileName;
            this.sendPostRequest(serverData, "server_vitrine/request/inventor_create.php").subscribe( (data:any) => {
                console.log(data);
            });            
        });        
    }

}