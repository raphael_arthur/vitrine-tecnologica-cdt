import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';
import { ServerService } from '../app/core/services/server.service';

@Injectable()

export class DepartmentService extends ServerService {

    public department_found:any;

    constructor(http:Http){
        super(http);
    }

    getAllDepartments():Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            this.sendPostRequest(serverData, "server_vitrine/request/department_getAll.php").subscribe( (data:JSON) => {
                console.log(data);
                this.department_found = JSON.parse(data['_body']);
                console.log(this.department_found);
                observer.next(true);
            });
            
        });
        
    }

    del(id:any):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'id=' + id;
            this.sendPostRequest(serverData, "server_vitrine/request/department_del.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    edit(department:any):Observable<any>{
        console.log(department);
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + department.nome + '&sigla=' + department.sigla + '&id=' + department.iddepartamento;
            this.sendPostRequest(serverData, "server_vitrine/request/department_edit.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    create(department:any):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + department.nome + '&sigla=' + department.sigla;
            this.sendPostRequest(serverData, "server_vitrine/request/department_create.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
        
    }

}