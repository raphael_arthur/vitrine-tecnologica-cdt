import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';
import { ServerService } from '../app/core/services/server.service';
@Injectable()
export class ServiceService extends ServerService {

    public service_found:any;

    constructor(http:Http){
        super(http);
    }

    getAllServices():Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            this.sendPostRequest(serverData, "server_vitrine/request/service_getAll.php").subscribe( (data:JSON) => {
                console.log(data['_body']);
                this.service_found = JSON.parse(data['_body']);
                console.log(this.service_found);
                observer.next(true);
            });
            
        });
        
    }

    del(id:any):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'id=' + id;
            this.sendPostRequest(serverData, "server_vitrine/request/service_del.php").subscribe( (data:any) => {
                console.log(data);
            });            
        });
    }

    edit(service:any):Observable<any>{
        console.log(service);
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + service.servico + '&id=' + service.idservicos;
            this.sendPostRequest(serverData, "server_vitrine/request/service_edit.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    create(service:any):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + service;
            this.sendPostRequest(serverData, "server_vitrine/request/service_create.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
        
    }

    getService_Lab(idlab:string):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "idlab=" + idlab;
            //console.log("serverDATA: " + serverData);
            this.sendPostRequest(serverData, "server_vitrine/request/service_getService_lab.php").subscribe( (data:JSON) => {
                console.log(data['_body']);
                this.service_found = JSON.parse(data['_body']);
                console.log(this.service_found);
                observer.next(true);
            });            
        });        
    }

}