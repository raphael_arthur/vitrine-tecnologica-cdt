import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';

import { ServerService } from '../app/core/services/server.service';

import { Service } from '../models/services';

@Injectable()
export class LabService extends ServerService {

    public lab_found:any;
    public has_dep:any;
    public has_inventor:any;
    public has_key:any;
    public has_service:any;
    public has_equip:any;

    constructor(http:Http){
        super(http);
    }

    getAllLabs():Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            this.sendPostRequest(serverData, "server_vitrine/request/lab_getAll.php").subscribe( (data:JSON) => {
                console.log(data);
                this.lab_found = JSON.parse(data['_body']);
                console.log(this.lab_found);
                observer.next(true);
            });
            
        });
        
    }

    search_by_service():Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            this.sendPostRequest(serverData, "server_vitrine/request/lab_getAllSearch.php").subscribe( (data:JSON) => {
                console.log(data);
                this.lab_found = JSON.parse(data['_body']);
                console.log(this.lab_found);
                observer.next(true);
            });
            
        });
        
    }

    del(id:any):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'id=' + id;
            this.sendPostRequest(serverData, "server_vitrine/request/lab_del.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    edit(lab:any, newDep:string, newInventor:string,newKey:string, equips:Array<string>,services:Array<Service>):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + lab.nome + "&descricao=" + lab.descricao + "&sigla=" + lab.sigla + "&iddepartamento=" + newDep + 
            "&idusuario=" + sessionStorage.getItem('userID') + "&coord_name=" + lab.coord_nome +
            "&coord_lattes=" + lab.coord_link + "&idpalavra_chave=" + newKey + "&idlab=" + lab.idlaboratorio + "&telefone=" + lab.telefone + "&contato=" + lab.contato + "&email=" + lab.email + "&endereco=" + lab.endereco + "&cidade=" + lab.cidade + "&UF=" + lab.UF + "&CEP=" + lab.CEP + "&idinventor=" + newInventor + "&servicos=" + JSON.stringify(services) + "&equipamentos=" + JSON.stringify(equips);;
            this.sendPostRequest(serverData, "server_vitrine/request/lab_edit.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    create(lab:any, services:Array<Service>, equips:Array<string>):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + lab.nome + "&desc=" + lab.desc + "&sigla=" + lab.sigla + "&iddepartamento=" + lab.iddepartamento + 
            "&idpalavra_chave=" + lab.idpalavra_chave + "&idusuario=" + sessionStorage.getItem('userID') + "&coord_name=" + lab.coord_nome +
            "&coord_lattes=" + lab.coord_lattes + "&telefone=" + lab.telefone + "&contato=" + lab.contato + "&email=" + lab.email + "&endereco=" + lab.endereco + "&cidade=" + lab.cidade + "&UF=" + lab.UF + "&CEP=" + lab.CEP + "&idinventor=" + lab.idinventor + "&servicos=" + JSON.stringify(services) + "&equipamentos=" + JSON.stringify(equips);

            this.sendPostRequest(serverData, "server_vitrine/request/lab_create.php").subscribe( (data:any) => {
                console.log(data);
            });            
        });        
    }

    getLab_has_dep(id:string):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "id=" + id;
            this.sendPostRequest(serverData, "server_vitrine/request/lab_getLab_has_dep.php").subscribe( (data:JSON) => {
                console.log(data);
                this.has_dep = JSON.parse(data['_body']);
                console.log(this.has_dep.departamento_iddepartamento);
                observer.next(true);
            });
        });
    }

    getLab_has_key(id:string):Observable<any>{
        console.log(id);
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "id=" + id;
            this.sendPostRequest(serverData, "server_vitrine/request/lab_getKey_has_lab.php").subscribe( (data:JSON) => {
                console.log(data);
                this.has_key = JSON.parse(data['_body']);
                console.log(this.has_key);
                observer.next(true);
            });
            
        });
        
    }

    getLab_has_service(id:string):Observable<any>
    {
        console.log(id);
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "id=" + id;
            //console.log("serverDATA: " + serverData);
            this.sendPostRequest(serverData, "server_vitrine/request/lab_getLab_has_service.php").subscribe( (data:JSON) => {
                console.log(data);
                this.has_service = JSON.parse(data['_body']);
                console.log(this.has_service);
                observer.next(true);
            });
            
        });
        
    }

    getLab_has_equip(id:string):Observable<any>
    {
        console.log(id);
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "id=" + id;
            //console.log("serverDATA: " + serverData);
            this.sendPostRequest(serverData, "server_vitrine/request/lab_getLab_has_equip.php").subscribe( (data:JSON) => {
                console.log(data);
                this.has_equip = JSON.parse(data['_body']);
                console.log(this.has_equip);
                observer.next(true);
            });
            
        });
        
    }

    
    getLab_has_inventor(id:string):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "id=" + id;
            this.sendPostRequest(serverData, "server_vitrine/request/lab_getLab_has_inventor.php").subscribe( (data:JSON) => {
                console.log(data);
                this.has_inventor = JSON.parse(data['_body']);//id_inventor possivelmente sera um array
                console.log(this.has_inventor);
                observer.next(true);
            });
            
        });
        
    }

    search_by_inventor():Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            this.sendPostRequest(serverData, "server_vitrine/request/lab_search_inventor.php").subscribe( (data:JSON) => {
                console.log(data);
                this.lab_found = JSON.parse(data['_body']);
                console.log(this.lab_found);
                observer.next(true);
            });
            
        });
    }

    search_by_key():Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            this.sendPostRequest(serverData, "server_vitrine/request/lab_search_key.php").subscribe( (data:JSON) => {
                console.log(data);
                this.lab_found = JSON.parse(data['_body']);
                console.log(this.lab_found);
                observer.next(true);
            });
            
        });
    }

    search_by_department():Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            this.sendPostRequest(serverData, "server_vitrine/request/lab_search_department.php").subscribe( (data:JSON) => {
                console.log(data);
                this.lab_found = JSON.parse(data['_body']);
                console.log(this.lab_found);
                observer.next(true);
            });            
        });
    }

    search_by_equip():Observable<any>
    {
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            //console.log("serverDATA: " + serverData);
            this.sendPostRequest(serverData, "server_vitrine/request/lab_search_equip.php").subscribe( (data:JSON) => {
                console.log(data);
                this.lab_found = JSON.parse(data['_body']);
                console.log(this.lab_found);
                observer.next(true);
            });
            
        });
    }

}