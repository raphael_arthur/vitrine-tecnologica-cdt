import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';
import { ServerService } from '../app/core/services/server.service';

@Injectable()

export class EquipmentService extends ServerService {

    public equipment_found:any;

    constructor(http:Http){
        super(http);
    }

    getAllEquipments():Observable<any>
    {
        console.log("estou dentro get ionic");
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            this.sendPostRequest(serverData, "server_vitrine/request/equipment_getAll.php").subscribe( (data:JSON) => {
                console.log(data['_body']);
                this.equipment_found = JSON.parse(data['_body']);
                console.log(this.equipment_found);
                observer.next(true);
            });
            
        });
        
    }

    del(id:any):Observable<any>
    {
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'id=' + id;
            this.sendPostRequest(serverData, "server_vitrine/request/equipment_del.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    edit(equipment:any):Observable<any>{
        console.log(equipment);
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + equipment.equipamento + '&id=' + equipment.idequipamento;
            this.sendPostRequest(serverData, "server_vitrine/request/equipment_edit.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    create(equipment:any):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + equipment.name + '&fileName='+equipment.fileName;
            this.sendPostRequest(serverData, "server_vitrine/request/equipment_create.php").subscribe( (data:any) => {
                console.log(data);
            });            
        });        
    }

    getLab(idlab:string):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "idlab=" + idlab;
            this.sendPostRequest(serverData, "server_vitrine/request/equipment_getLab.php").subscribe( (data:JSON) => {
                console.log(data['_body']);
                this.equipment_found = JSON.parse(data['_body']);
                console.log(this.equipment_found);
                observer.next(true);
            });            
        });        
    }

}