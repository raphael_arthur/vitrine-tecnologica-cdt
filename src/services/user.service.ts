import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';
import { ServerService } from '../app/core/services/server.service';


@Injectable()
export class UserService extends ServerService {

    public userData:any;

    constructor(http:Http){
        super(http);
    }

    createUser(formData:string):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            let serverData:string = 'firstName=' + formData['firstName'] + '&lastName=' + formData['lastName'] + '&email=' + formData['email'] + '&password=' + formData['password'] + '&ramal=' + 
                formData['ramal'] + '&setor=' + formData['setor'] + '&userLvl=' + formData['userLvl'];
            this.sendPostRequest(serverData, "server_vitrine/request/signup.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
        
    }

    getUser(user_id:string):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'user_id=' + user_id;
            this.sendPostRequest(serverData, "server_vitrine/request/user_get.php").subscribe( (data:JSON) => {
                console.log(data);
                this.userData = JSON.parse(data['_body']);
                console.log(this.userData);
                observer.next(true);
            });            
        });
    }

    editUser(userData:any):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'firstName=' + userData.nome + '&lastName=' + userData.sobrenome + '&ramal=' + 
                userData.ramal + '&senha=' + userData.newPassword +'&email=' +userData.email;
            this.sendPostRequest(serverData, "server_vitrine/request/user_edit.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    public SendMail(email:string):void{
        let body = 'email='+email;
        let url = "server_vitrine/request/RecuperarSenha.php";
        this.sendPostRequest(body,url).subscribe( (data:any) => {
            console.log(data);
        });
    }

    public FindUserByEmail(email:string):Observable<any>{
        return Observable.create ( (observer:Observer<any>) =>{
            let url = "server_vitrine/request/find_user_by_mail.php";
            let serverData:string = 'email='+email;
            this.sendPostRequest(serverData,url).subscribe((data:any) =>{
                console.log(data);
                observer.next(data);
            });
        });
    }
    
}
