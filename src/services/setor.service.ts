import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Observer } from 'rxjs/Rx';
import { ServerService } from '../app/core/services/server.service';
@Injectable()
export class SetorService extends ServerService {

    public setor_found:any;

    constructor(http:Http){
        super(http);
    }

    getAllSetores():Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = "";
            this.sendPostRequest(serverData, "server_vitrine/request/setor_getAll.php").subscribe( (data:JSON) => {
                console.log(data);
                this.setor_found = JSON.parse(data['_body']);
                console.log(this.setor_found);
                observer.next(true);
            });
            
        });
        
    }

    del(id:any):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'id=' + id;
            this.sendPostRequest(serverData, "server_vitrine/request/setor_del.php").subscribe( (data:any) => {
                console.log(data);
            });            
        });
    }

    edit(setor:any):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + setor.nome + '&id=' + setor.idsetor;
            this.sendPostRequest(serverData, "server_vitrine/request/setor_edit.php").subscribe( (data:any) => {
                console.log(data);
            });
            
        });
    }

    create(setor:string):Observable<any>{
        return Observable.create( (observer:Observer<any>) => { 
            //configure form data to be sended to server
            let serverData:string = 'name=' + setor;
            this.sendPostRequest(serverData, "server_vitrine/request/setor_create.php").subscribe( (data:any) => {
                console.log(data);
            });            
        });        
    }

}