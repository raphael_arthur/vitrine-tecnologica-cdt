import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Projects } from '../projects/projects';
import { TecnologyProject } from '../tecnology-project/tecnology-project';
import { LoginPage } from '../login/login';

@IonicPage()
@Component({
  selector: 'page-services',
  templateUrl: 'services.html',
})
export class Services {

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams
    )
  {
  }

  goProjects():void{
    this.navCtrl.push(Projects);
  }

  public goServices():void{
    this.navCtrl.push(Services);
  }

  goTecnologyProject():void{
    this.navCtrl.push(TecnologyProject);
  }

  goToLogin():void{
    this.navCtrl.push(LoginPage);
  }
}
