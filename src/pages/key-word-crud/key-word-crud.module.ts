import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KeyWordCrudPage } from './key-word-crud';

@NgModule({
  declarations: [
    KeyWordCrudPage,
  ],
  imports: [
    IonicPageModule.forChild(KeyWordCrudPage),
  ],
  exports: [
    KeyWordCrudPage
  ]
})
export class KeyWordCrudPageModule {}
