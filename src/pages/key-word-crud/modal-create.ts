import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform, ViewController,AlertController } from 'ionic-angular';
import { FileChooser } from '@ionic-native/file-chooser';

import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

import {ModalUploadFile} from  './modal-upload'
import {ModalCreateLab} from  '../lab-crud/modal-create'

//------Services-----------
import { TechService } from '../../services/tecnology.service';
import { InventorService } from '../../services/inventor.service';
import { DepartmentService } from '../../services/department.service';
import { KeyService } from '../../services/key.service';
import { LabService } from '../../services/laboratory.service';
import { CategoryService } from '../../services/category.service'

@Component({
    selector: 'modal-create',
    templateUrl: 'modal-create.html',
  })
  
export class ModalCreateKeyWord{

    private category_observer:any;
    private categories:any;
    private newKey:string;
    private newFk:string;
        
    constructor(
        public viewCtrl:ViewController,
        public platform: Platform,
        public navParams: NavParams,
        private categoryService:CategoryService, 
        private inventorService:InventorService,
        private teacherService:InventorService,
        private departmentService: DepartmentService,
        private keyService: KeyService,
    ){
        this.newKey = "";
        this.newFk = "";    
        this.GetCategories();
    }

    public GetCategories(){
        this.category_observer = this.categoryService.getAllCategories().subscribe( data => {
            console.log("recebendo as categorias");
            this.categories = this.categoryService.category_found;
        });    
    }

    public Create(key:string,idCat:string):void{
        this.keyService.create(key,idCat).subscribe( data => 
        {
            console.log("criando nova palavra chave");
        });        
        this.viewCtrl.dismiss();
    }

    public dismiss() {
        this.viewCtrl.dismiss();
      }

}