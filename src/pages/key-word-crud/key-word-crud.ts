import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,ModalController } from 'ionic-angular';
import { CategoryService } from '../../services/category.service';
import { KeyService } from '../../services/key.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalCreateKeyWord } from './modal-create';

@IonicPage()
@Component({
  selector: 'page-key-word-crud',
  templateUrl: 'key-word-crud.html',
})
export class KeyWordCrudPage {

  private category_observer:any;
  private categories:any;
  private key_observer:any;
  private keys:any;
  private newKey:string;
  private newFk:string;


  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl:ViewController,
    private categoryService:CategoryService, 
    private keyService:KeyService, 
    private formBuilder:FormBuilder,
    private modalAdd:ModalController
  ) 
  {
    this.newKey = "";
    this.newFk = "";
    this.getKey();
    this.getCategories();

  }

  public doRefresh(refresher) {
    this.getCategories();
    this.getKey();
    this.navCtrl.push(this.navCtrl.getActive().component).then(() => {
      let index = this.viewCtrl.index;
      this.navCtrl.remove(index);
   })
  }

  private getCategories():void{
    this.category_observer = this.categoryService.getAllCategories().subscribe( data => {
        this.categories = this.categoryService.category_found;
    });
  }

  private getKey():void{
    this.key_observer = this.keyService.getAllKeys().subscribe( data => {
        this.keys = this.keyService.key_found;
    });
  }

  private del(id):void{
    this.keyService.del(id).subscribe( data => {
        console.log("dentro do subscribe");
        this.keys = this.keyService.key_found;
    });
    this.doRefresh(0);
  }
  
  private edit(key:any):void{
    this.keyService.edit(key);
    this.doRefresh(0);
  }

  private create(key:string,idCat:string):void{
    console.log(this.newKey);
    this.keyService.create(key,idCat);
    this.doRefresh(0);
  }

  private ShowModalPage(){
      let modal =  this.modalAdd.create(ModalCreateKeyWord);
      modal.onDidDismiss(data =>{
        this.doRefresh(0);
      })
      modal.present();
  }

    /*
  
  Guilherme Natal 23/02/2018
  A função Logout desloga o usário do banco de dados
  e retorna a página inícial
  */

  public Logout():void{
    this.BackToInitialPage();
  }
  public BackToInitialPage():void{
    location.reload();
    this.DeleteAllCookies();
  }

/*
Guilherme Natal 23/02/2018
A função DeleteAllCookies deleta os cookies para que o usuário não
logue altomaticamente.
*/

  public DeleteAllCookies() {
      var cookies = document.cookie.split(";");

      for (var i = 0; i < cookies.length; i++) {
          var cookie = cookies[i];
          var eqPos = cookie.indexOf("=");
          var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
          document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
      }
  }


}
