import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform, ViewController, AlertController } from 'ionic-angular';
import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';
import { ModalCreateKeyWord } from '../key-word-crud/modal-create';

import { Service } from '../../models/services';
//------Services-----------
import { LabService } from '../../services/laboratory.service';
import { InventorService } from '../../services/inventor.service';
import { DepartmentService } from '../../services/department.service';
import { KeyService } from '../../services/key.service'
import { FileUploadService } from '../../services/UploadFileService';


@Component({
  selector: 'modal-create',
  templateUrl: 'modal-create.html',
})

export class ModalCreateLab {

  private newServices:Array<Service>;
  private newEquip:Array<string>;

  private lab_observer:any;
  private labs:any;
  private newLab:any;

  private department_observer:any;
  private departments:any;

  private key_observer:any;
  private keys:any;

  private teacher_observer:any;
  private teachers:any;
  private newTeacher:any;

  public fileToUpload:File = null;

  constructor(
    public viewCtrl: ViewController, 
    public platform: Platform, 
    public navParams: NavParams, 
    private labService:LabService, 
    private teacherService:InventorService, 
    private departmentService: DepartmentService, 
    private keyService: KeyService,
    private keyWordModal:ModalController,
    public inventorRegister:AlertController,
    public fileUploadService:FileUploadService
  ) 
  {
    this.newLab = 
    {
      'nome':  "",
      'desc': "",
      'sigla': "",
      'coord_nome':"",
      'coord_lattes':"",
      'idinventor': "",
      'iddepartamento':"",
      'idpalavra_chave':"",
      'telefone':"",
      'contato':"",
      'email':"",
      'endereco':"",
      'cidade':"",
      'UF':"",
      'CEP':"",
      "fileName":""
    };

    this.newServices = [];
    this.newEquip = [];

    let temp = new Service();
    this.newServices.push(temp);

    let emptyStr = "";
    this.newEquip.push(emptyStr);

    this.newTeacher = 
    {
      'nome':  "",
      'lattes': ""
    };

    this.getTeachers();
    this.getDepartments();
    this.getKeys();

  }

  private getTeachers():void
  {
    this.teacher_observer = this.teacherService.getAllInventores().subscribe( data => {
      this.teachers = this.teacherService.inventor_found;
    });
  }

  private getDepartments():void{
    this.department_observer = this.departmentService.getAllDepartments().subscribe( data => {
        this.departments = this.departmentService.department_found;
    });
  }

  private getKeys():void{
    this.key_observer = this.keyService.getAllKeys().subscribe( data => {
        this.keys = this.keyService.key_found;
    });
  }


  /*
  Guilherme Natal 13/03/2018
  A função HandleFileInput pega a o arquivo escolhido do formulario
  e coloca em fileToUpload
  */

  public HandleFileInput(files: FileList) {
    this.fileToUpload = document.forms['formImage']['file'].files[0];
  }

  private create(lab:any):void
  {
    let body = new FormData();
    let hasFile:boolean = false;
    if(this.fileToUpload){
      hasFile = true;
      body.append('file',this.fileToUpload,this.fileToUpload.name);
    }
    if(hasFile){
      lab.fileName = this.fileToUpload.name;
      this.fileUploadService.PostFile(body,'http://localhost:8080/server_vitrine/request/upload_file.php').subscribe(data => {
        console.log(data);
      }, error => {
        console.log(error);
      });  
    }

    this.labService.create(lab, this.newServices, this.newEquip).subscribe( data => 
    {
      console.log("dentro do subscribe");
    });
    this.dismiss();
  }

  public dismiss() {
    this.viewCtrl.dismiss();
  }

  private AddKeyWord(keyWord:any):void{
    if(this.newLab.idpalavra_chave.indexOf(keyWord) < 0){
      this.newLab.idpalavra_chave += keyWord+"|";
      console.log(this.newLab.idpalavra_chave);
    }else{
      this.newLab.idpalavra_chave = this.newLab.idpalavra_chave.replace(keyWord+"|","");
      console.log(this.newLab.idpalavra_chave);
    }
  }

  private registerInventor():void{
    let controller = this.inventorRegister.create({
      title:'Cadastro Professor',
      message: 'Entre com o nome do Professor',
      inputs:[{
        name:'nome',
        placeholder:'inventor'
      },
      {
        name:'lattes',
        placeholder:'Lattes(copiar e colar o link)'
      }
      ],
      buttons:[{
        text: 'Confirmar',
        handler: data => {
          this.newTeacher.nome = data.nome;
          this.newTeacher.lattes = data.lattes;
          this.teacherService.create(this.newTeacher).subscribe(data => {
              console.log("inventor");
          })
        } 
      }
      ]
    });
    controller.present();
  }

  public ShowKeyWordModalPage(){
      let modal = this.keyWordModal.create(ModalCreateKeyWord);
      modal.onDidDismiss(data => {this.dismiss()});
      modal.present();
  }

  private AddInventor(inventor:any):void{
    if(this.newLab.idinventor.indexOf(inventor) < 0){//if is not selected add at click
      this.newLab.idinventor += inventor+"|";
      console.log(this.newLab.idinventor);
    }else{//if is selected erase at click
      this.newLab.idinventor = this.newLab.idinventor.replace(inventor+"|","");
      console.log(this.newLab.idinventor);
    }
  }

  private AddDepartamento(departamento:any):void{
    if(this.newLab.iddepartamento.indexOf(departamento) < 0){
      this.newLab.iddepartamento += departamento+"|";
      console.log(this.newLab.iddepartamento);
    }else{
      this.newLab.iddepartamento = this.newLab.iddepartamento.replace(departamento+"|","");
      console.log(this.newLab.iddepartamento);
    }
  }

  /*Adicionar Novo Serviço Ao Laboratório*/
  public addNewSubField(k:number):void
  {
    let temp = "";
    this.newServices[k].subname.push(temp);
  }

  public removeSubField(k:number, j:number):void
  {
    this.newServices[k].subname.splice(j,1);
  }

  public addNewField():void
  {
    let temp = new Service();
    this.newServices.push(temp);
  }

  public removeField(k:number):void
  {
    this.newServices.splice(k,1);
  }

  public addNewEquip():void
  {
    let temp = "";
    this.newEquip.push(temp);
  }

  public removeFieldEquip(k:number):void
  {
    this.newEquip.splice(k,1);
  }
}
