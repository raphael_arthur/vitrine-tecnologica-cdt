import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform, ViewController, AlertController } from 'ionic-angular';

//------Models-------------
import { Service } from '../../models/services';
//------Services-----------
import { LabService } from '../../services/laboratory.service';
import { DepartmentService } from '../../services/department.service';
import { KeyService } from '../../services/key.service'
import { InventorService } from '../../services/inventor.service';
import { EquipmentService } from '../../services/equipment.service';
import { ServiceService } from '../../services/service.service';

@Component({
  selector: 'modal-edit',
  templateUrl: 'modal-edit.html',
})
export class ModalEditLab {

  private department_observer:any;
  private departments:any;
  private has_dep:any;
  private departments_checked:any;
  private department_id_list:string;

  private key_observer:any;
  private keys:any;
  private has_key:any;
  private keys_checked:any;
  private keys_id_list:string;
  
  private lab:any;
  private oldDepartment:string;
  private oldInventor:string;
  private oldKey:string;
  private oldUser:string;

  private teacher_observer:any;
  private teachers:any;
  private has_teacher:any;
  private teachers_checked:any;
  private newTeacher:any;
  private inventor_id_list:string;

  private equip_observer:any;
  private equips:Array<string>;
  private has_equip:any;

  private service_observer:any;
  private services:Array<Service>;

  constructor(
    public viewCtrl: ViewController, 
    public platform: Platform, 
    public navParams: NavParams, 
    private labService:LabService, 
    private departmentService: DepartmentService, 
    private keyService: KeyService,
    private teacherService:InventorService,
    private inventorService:InventorService,
    public inventorRegister:AlertController,
    private equipService:EquipmentService,
    private serviceService:ServiceService,

  ) 
  {
    this.newTeacher = {
      'nome':  "",
      'lattes': ""
    };


    this.lab = this.navParams.get('lab');
    console.log(this.lab);

    this.getTeachers();
    this.getDepartments();
    this.getKeys();

    this.getLab_has_key(this.lab.idlaboratorio);
    this.getLab_has_dep(this.lab.idlaboratorio);
    this.getLab_has_inventor(this.lab.idlaboratorio);

    this.getEquip_lab(this.lab.idlaboratorio);
    this.getService_lab(this.lab.idlaboratorio);
    this.InitLists();
  }


  private getTeachers():void
  {
    this.teacher_observer = this.teacherService.getAllInventores().subscribe( data => {
        this.teachers = this.teacherService.inventor_found;
    });
  }

  private getDepartments():void
  {
    this.department_observer = this.departmentService.getAllDepartments().subscribe( data => {
        console.log("dentro do subscribe");
        this.departments = this.departmentService.department_found;
    });
  }

  private getKeys():void
  {
    this.key_observer = this.keyService.getAllKeys().subscribe( data => {
        console.log("dentro do subscribe");
        this.keys = this.keyService.key_found;
    });
  }

  private edit(lab:any):void
  {
    this.oldUser = this.lab.usuario_idusuario;
    this.labService.edit(lab, 
      this.department_id_list,
      this.inventor_id_list,
      this.keys_id_list,
      this.equips,
      this.services,
      ).subscribe( data => 
    {
      console.log("dentro do subscribe");
    });
  }

  private getLab_has_key(id:string):void{
    this.department_observer = this.labService.getLab_has_key(id).subscribe( data => {
        console.log("dentro do subscribe");
        this.has_key = this.labService.has_key;
        this.oldKey = this.has_key.palavra_chave_idpalavra_chave;
        this.CheckLabKeys();
        this.CreateLabKeysList();
    });
  }

  /*
  Guilherme natal 20/02/2018
  a função CheckTechKeys verifica quais chaves estão
  presentes na tecnologia e coloca em array para ser utiliza no html
  para marcar os checkbox
  */

  private CheckLabKeys():void{
    let checked = 0;

    if(this.keys != null){

      for(let j=0; j < this.keys.length; j++)
      {
        checked = 0;
        for(let i = 0; i < this.has_key.length; i++)
        {
          if(this.has_key[i].palavra_chave_idpalavra_chave == this.keys[j].idpalavra_chave)
          {
            let temp = [{"idpalavra_chave": this.keys[j].idpalavra_chave, "nome":this.keys[j].nome, "checked":"true"}]
            checked = 1;
            this.keys_checked.push(temp[0]);
            break;
          }
        }
        if(checked == 0)
        {
          let temp = [{"idpalavra_chave": this.keys[j].idpalavra_chave, "nome":this.keys[j].nome, "checked":"false"}]
          this.keys_checked.push(temp[0]);
        }
      }
    }
  }

  /*
  guilherme natal 20/02/2018
  a função CreateTechKeyList cria uma lista separada por |
  contendo os ids de cada palavra chave para posteriormente fazer o
  update no banco de dados
  */

  private CreateLabKeysList():void{
    for(let key_check of this.keys_checked){
      if(key_check.checked == "true")
        this.keys_id_list += key_check.idpalavra_chave + "|";
    }
    console.log(this.keys_id_list);
  }


  private getLab_has_dep(id:string):void{
    this.department_observer = this.labService.getLab_has_dep(id).subscribe( data => {
        this.has_dep = this.labService.has_dep;
        this.oldDepartment = this.has_dep.departamento_iddepartamento;
        this.CheckDept();
        this.CreateDeptList();
    });
  }

  /*
  guilherme natal 21/02/2018
  a função CheckDept percorre todos os ids de depts do banco de dados
  e compara com os ids de dept da tecnologia em questão para marcar no html
  */


  private CheckDept():void{
    let checked = false;

    if(this.departments != null){

      for(let j=0; j < this.departments.length; j++)
      {
        checked = false;
        for(let i = 0; i < this.has_dep.length; i++)
        {
          if(this.has_dep[i].departamento_iddepartamento == this.departments[j].iddepartamento)
          {
            let temp = [{"iddepartamento": this.departments[j].iddepartamento, "nome":this.departments[j].nome, "checked":"true"}]
            checked = true;
            this.departments_checked.push(temp[0]);
            break;
          }
        }
        if(!checked)
        {
          let temp = [{"iddepartamento": this.departments[j].iddepartamento, "nome":this.departments[j].nome, "checked":"false"}]
          this.departments_checked.push(temp[0]);
        }
      }
    }
  }

  /*
  guilherme natal 21/02/2018
  a função CreateDeptList cria uma lista separada por | de 
  ids de departamentos na tecnologia.
  */

  private CreateDeptList():void{
    for(let deptChecked of this.departments_checked){
      if(deptChecked.checked =="true")
        this.department_id_list += deptChecked.iddepartamento + "|";
    }
    console.log(this.department_id_list);
  }

  private RegisterInventor():void{
    let controller = this.inventorRegister.create({
      title:'Cadastro Professor',
      message: 'Entre com o nome do do inventor',
      inputs:[{
        name:'nome',
        placeholder:'inventor'
      },
      {
        name:'lattes',
        placeholder:'Lattes(copiar e colar o link)'
      }
      ],
      buttons:[{
        text: 'Confirmar',
        handler: data => {
          this.newTeacher.nome = data.nome;
          this.newTeacher.lattes = data.lattes;
          this.inventorService.create(this.newTeacher).subscribe(data => {
              console.log("inventor");
          })
        } 
      }
      ]
    });
    controller.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  private getLab_has_inventor(id:string):void{
    this.teacher_observer = this.labService.getLab_has_inventor(id).subscribe( data => {
        console.log("dentro do subscribe");
        this.has_teacher = this.labService.has_inventor;
        this.oldInventor = this.has_teacher.inventor_idinventor;
        this.CheckInventor();
        this.CreateInventorList();
    });    
  }


  /*
  guilherme natal 21/02/2018
  a função checkInventor percorre toda a lista de inventores do banco de dados
  e a lista de inventores da tecnologia em questão para marcar no html
  */

  private CheckInventor():void{
    let checked = 0; 
    if(this.teachers != null){

      for(let j=0; j < this.teachers.length; j++)
      {
        checked = 0;
        for(let i = 0; i < this.has_teacher.length; i++)
        {
          if(this.has_teacher[i].inventor_idinventor == this.teachers[j].idinventor)
          {
            let temp = [{"idinventor": this.teachers[j].idinventor, "nome":this.teachers[j].nome, "checked":"true"}]
            checked = 1;
            this.teachers_checked.push(temp[0]);
            break;
          }
        }
        if(checked == 0)
        {
          let temp = [{"idinventor": this.teachers[j].idinventor, "nome":this.teachers[j].nome, "checked":"false"}]
          this.teachers_checked.push(temp[0]);
        }
      }
    }
    console.log(this.teachers_checked);

  }


  /*
  guilherme natal 21/02/2018
  a função CreateInventorList cria uma lista separada por | 
  de ids de teachers presentes na tecnologia. 
  Ex: 1|2|3|  (perceba a barra no final)
  */

  private CreateInventorList():void{
    for(let inventor_checked of this.teachers_checked){
      if(inventor_checked.checked =="true")
        this.inventor_id_list += inventor_checked.idinventor +"|";
    } 
    console.log(this.inventor_id_list);
  }

  private InitLists():void{
    this.keys_id_list = "";
    this.inventor_id_list = "";
    this.department_id_list = "";


    this.has_dep = "";
    this.departments_checked = [];
    this.keys_checked = [];
    this.teachers_checked = [];

    this.equips = []; 
    this.services = [];
  }

  private AddRemoveKeyWord(id:string):void {
    if(this.keys_id_list.indexOf(id) < 0){
        this.keys_id_list += id+"|";
        console.log(this.keys_id_list);
    }else{
      this.keys_id_list = this.keys_id_list.replace(id + "|","");
      console.log(this.keys_id_list);
    }
  }

  private AddRemoveDept(id:string):void{    
    if(this.department_id_list.indexOf(id) < 0){//if is not selected add at click
      this.department_id_list += id+"|";
      console.log(this.department_id_list);
    }else{//if is selected erase at click
      this.department_id_list = this.department_id_list.replace(id+"|","");
      console.log(this.department_id_list);
    }
  }

  private AddRemoveInventor(id:string):void{
    if(this.inventor_id_list.indexOf(id) < 0){
      this.inventor_id_list += id + "|";
      console.log(this.inventor_id_list + "Added");
    }else{
      this.inventor_id_list =  this.inventor_id_list.replace(id+"|","");
      console.log(this.inventor_id_list + "removed");
    }

  }

  /*Raphael Resende 07/03/18
  Funçao getEquip_lab retorna os equipamentos pertencentes a um laboratório
  @param idlab -> é o id (string) do laboratório que deseja-se saber os equipamentos pertencentes
  */

  private getEquip_lab(idlab:string):void{
    this.department_observer = this.equipService.getLab(idlab).subscribe( data => {
        console.log("dentro do equip_lab");
        this.equip_observer = this.equipService.equipment_found;

        for(let equip of this.equip_observer)
        {
          this.equips.push(equip.equipamento);
        }
        console.log(this.equips);
    });
  }
  public addNewEquip():void
    {
      let temp = "";
      this.equips.push(temp);
    }

  public removeFieldEquip(k:number):void
  {
    this.equips.splice(k,1);
  }

  private getService_lab(idlab:string):void
  {
    this.department_observer = this.serviceService.getService_Lab(idlab).subscribe( data => {
      console.log("dentro do service_lab");
      this.service_observer = this.serviceService.service_found;
      console.log(this.service_observer);
      for(let service of this.service_observer)
      {
        let temp = new Service();
        if(this.services.length == 0)
        {
          temp.name = service.servico;
          temp.subname[0] = service.subservico;
          this.services.push(temp);
          console.log("caralho");
        }
        else
        {
          let flag = 0; //Flag para adicionar serviços no vetor;
          for(let i = 0; i < this.services.length; i++)
          {
            if(this.services[i].name == service.servico)
            {
              for(let j = 0; j < this.services[i].subname.length; j++)
              {
                if(this.services[i].subname[j] != service.subservico)
                {
                  let temp2 = service.subservico;
                  this.services[i].subname.push(temp2);
                  console.log("porra");
                }
              }
              flag = 0;
            }
            else
            {
              flag = 1;
            }
          }
          if(flag)
          {
            temp = new Service();
            temp.name = service.servico;
            temp.subname[0] = service.subservico;
            this.services.push(temp);
            console.log("vaaaaaai");
          }
        }

        
        /*for(let sub of service.subservico)
        this.equips.push(equip.equipamento);*/
        console.log(service);
      }
      console.log(this.services);
    });
  console.log(this.services);
  }

  /*Adicionar Novo Serviço Ao Laboratório*/
  public addNewSubField(k:number):void
  {
    let temp = "";
    this.services[k].subname.push(temp);
  }

  public removeSubField(k:number, j:number):void
  {
    this.services[k].subname.splice(j,1);
  }

  public addNewField():void
  {
    let temp = new Service();
    this.services.push(temp);
  }

  public removeField(k:number):void
  {
    this.services.splice(k,1);
  }
}