import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LabCrudPage } from './lab-crud';

@NgModule({
  declarations: [
    LabCrudPage,
  ],
  imports: [
    IonicPageModule.forChild(LabCrudPage),
  ],
  exports: [
    LabCrudPage
  ]
})
export class LabCrudPageModule {

  
}
