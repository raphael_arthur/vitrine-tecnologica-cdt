import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,ViewController } from 'ionic-angular';
import { LabService } from '../../services/laboratory.service';
import { ModalCreateLab } from './modal-create';
import { ModalEditLab } from './modal-edit';

@IonicPage()
@Component({
  selector: 'page-lab-crud',
  templateUrl: 'lab-crud.html',
})
export class LabCrudPage {

  private lab_observer:any;
  private labs:any;
  private newLab:any;

  private department_observer:any;
  private departments:any;

  private key_observer:any;
  private keys:any;

  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    private labService:LabService
  ) 
  {
    this.newLab = 
    {
      'nome':  "",
      'desc': "",
      'sigla': "",
      'coord_nome':"",
      'coord_lattes':"",
      'idinventor': "",
      'iddepartamento':"",
      'idpalavra_chave':"",
      'telefone':"",
      'contato':"",
      'email':"",
      'endereco':"",
      'cidade':"",
      'UF':"",
      'CEP':"",
      
    };
    this.getLabs();

  }
  
  public doRefresh(refresher) {
    this.getLabs();
    this.navCtrl.push(this.navCtrl.getActive().component).then(() => {
      let index = this.viewCtrl.index;
      this.navCtrl.remove(index);
   })
  }


  private getLabs():void{
    this.lab_observer = this.labService.getAllLabs().subscribe( data => {
        this.labs = this.labService.lab_found;
    });
  }

  private del(id):void{
    this.labService.del(id).subscribe( data => {
        this.labs = this.labService.lab_found;
    });
    this.doRefresh(0);
  }
  
  private modalEdit(lab:any){
    let modal = this.modalCtrl.create(ModalEditLab, {lab:lab});
    modal.present();
  }


  private modalCreate(){
    let modal = this.modalCtrl.create(ModalCreateLab);
    modal.onDidDismiss(data =>{
      this.doRefresh(0);
    });
    modal.present();
  }

  /*
  
  Guilherme Natal 23/02/2018
  A função Logout desloga o usário do banco de dados
  e retorna a página inícial
  */

  public Logout():void{
    this.BackToInitialPage();
  }
  public BackToInitialPage():void{
    location.reload();
    this.DeleteAllCookies();
  }

/*
Guilherme Natal 23/02/2018
A função DeleteAllCookies deleta os cookies para que o usuário não
logue altomaticamente.
*/

  public DeleteAllCookies() {
      var cookies = document.cookie.split(";");

      for (var i = 0; i < cookies.length; i++) {
          var cookie = cookies[i];
          var eqPos = cookie.indexOf("=");
          var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
          document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
      }
  }
}
