import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//-----------------Services-------------------------------
import { LabService } from '../../services/laboratory.service';
import { Services } from '@angular/core/src/view';
import { TecnologyProject } from '../tecnology-project/tecnology-project';
import { Projects } from '../projects/projects';
import { LoginPage } from '../login/login';
import { TecnologyDescPage } from '../tecnology-desc/tecnology-desc';

//------------------PAGES----------------------------------------
import { ServiceDescPage } from '../service-desc/service-desc';
/**
 * Generated class for the ServiceSearchPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-service-search',
  templateUrl: 'service-search.html',
})
export class ServiceSearchPage {

  private lab_observer:any;
  private labs:any;

  private keys: any;
  private departments: any;
  private services:any;
  private equips:any;

  private id_ext:any; //id_ext = parametro de busca da página inicial

  private searchQuery: string = '';
  
  private labs_bkp:any;

  private isEquip:boolean;
  private isLab:boolean;
  private isService: boolean;
  private isTeacher: boolean;
  private isKey: boolean;
  private isDepartment: boolean; 
  private lab_filter:any;

  private filter_inventor:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public labService:LabService) {
    //id_ext armazena a opção de busca que o usuário entrou pela tela principal: 1->Equipamento, 2->nome do lab, 3->Serviço prestado, 
    //essas informações serão input pra checkbox
    this.id_ext = this.navParams.get('id');

    if(this.id_ext == 1)
    {
      this.isEquip = true;
      this.isLab = false;
      this.isService = false;
    }
    else if(this.id_ext == 2)
    {
      this.isEquip = false;
      this.isLab = true;
      this.isService = false;
    }
    else if(this.id_ext == 3)
    {
      this.isEquip = false;
      this.isLab = false;
      this.isService = true;
    }
    else
    {
      this.isEquip = false;
      this.isLab = false;
      this.isService = false;
    }
    this.isTeacher = false;
    this.isKey = false;
    this.isDepartment = false;

    this.getLabs();
    this.search_by_inventor();
    this.search_by_key();
    this.search_by_department();
    this.search_by_service();
    this.search_by_equip();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServiceSearchPage');
  }

  private goToLab(lab:any):void
  {
    console.log("antes");
    this.navCtrl.push(ServiceDescPage, {lab:lab});
    console.log("depois");
  }

  private getLabs():void{
    this.lab_observer = this.labService.getAllLabs().subscribe( data => {
        console.log("dentro do subscribe");
        this.labs = this.labService.lab_found;
        this.labs_bkp = this.labs;
        console.log(this.labs);
    });
  }

  private search_by_inventor(){
    this.lab_observer = this.labService.search_by_inventor().subscribe( data => {
      this.filter_inventor = this.labService.lab_found;
    });
  }

  private search_by_key(){
    this.lab_observer = this.labService.search_by_key().subscribe( data => {
      this.keys = this.labService.lab_found;
    });
  }

  private search_by_department(){
    this.lab_observer = this.labService.search_by_department().subscribe( data => {
      this.departments = this.labService.lab_found;
    });
  }

  private search_by_service(){
    this.lab_observer = this.labService.search_by_service().subscribe( data => {
      this.services = this.labService.lab_found;
    });
  }

  private search_by_equip()
  {
    this.lab_observer = this.labService.search_by_equip().subscribe( data => {
      this.equips = this.labService.lab_found;
    });
  }
  
  private getItems(ev: any){
    console.log(this.labs);
    this.labs = this.labs_bkp;
    let val = ev.target.value;
    let labs2 = [];
    let append = 0;
    
    console.log(this.isEquip);
    
    console.log(this.isTeacher);
    console.log(this.isKey);
    console.log(this.isDepartment);
    this.lab_filter = [];

    if (val && val.trim()!= '')
    {
      //Filtro por Inventor
      if(this.isTeacher)
      {
        console.log(this.filter_inventor);
        console.log(val);
        let results = this.filter_inventor.filter((inventor) => {
          return (inventor.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
        console.log(results);
        if(results.length > 0)
        {
          for(let i = 0; i < results.length; i++)
          {
            for(let j = 0; j < this.labs.length; j++)
            {
              if(results[i].laboratorio_idlaboratorio == this.labs[j].idlaboratorio)
              {
                this.lab_filter.push(this.labs[j]);
              }
            }
            
          }
        }
        console.log(this.lab_filter);
      }
      //-------------------------------------------------------------------------------
      //Filtrar por Palavra-chave
      if(this.isKey)
      {
        console.log(this.keys);
        let results = this.keys.filter((key) => {
          return (key.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
        console.log(results);
        if(results.length > 0)
        {
          for(let i = 0; i < results.length; i++)
          {
            for(let j = 0; j < this.labs.length; j++)
            {
              if(results[i].laboratorio_idlaboratorio == this.labs[j].idlaboratorio)
              {
                this.lab_filter.push(this.labs[j]);
              }
            }
            
          }
        }
        console.log(this.lab_filter);
      }
      //----------------------------------------------------------------------------------
      //Filtro por Departamento
      if(this.isDepartment)
      {
        console.log(this.departments);
        let results = this.departments.filter((department) => {
          return (department.nome.toLowerCase().indexOf(val.toLowerCase()) > -1 || department.sigla.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
        console.log(results);
        if(results.length > 0)
        {
          for(let i = 0; i < results.length; i++)
          {
            for(let j = 0; j < this.labs.length; j++)
            {
              if(results[i].laboratorio_idlaboratorio == this.labs[j].idlaboratorio)
              {
                this.lab_filter.push(this.labs[j]);
              }
            }
            
          }
        }
        console.log(this.lab_filter);
      }
      //----------------------------------------------------------------------------------
      //Filtro por serviço
      if(this.isService)
      {
        console.log(this.services);
        let results = this.services.filter((lab) => {
          return (lab.servico.toLowerCase().indexOf(val.toLowerCase()) > -1 || lab.subservico.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
        console.log(results);
        if(results.length > 0)
        {
          for(let i = 0; i < results.length; i++)
          {
            for(let j = 0; j < this.labs.length; j++)
            {
              if(results[i].idlaboratorio == this.labs[j].idlaboratorio)
              {
                this.lab_filter.push(this.labs[j]);
              }
            }
            
          }
        }
        console.log(this.lab_filter);
      }
      //----------------------------------------------------------------------------------
      //Filtro por equipamento
      if(this.isEquip)
      {
        console.log(this.equips);
        console.log(val);
        let results = this.equips.filter((equip) => {
          return (equip.equipamento.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
        console.log(results);
        if(results.length > 0)
        {
          for(let i = 0; i < results.length; i++)
          {
            for(let j = 0; j < this.labs.length; j++)
            {
              if(results[i].idlaboratorio == this.labs[j].idlaboratorio)
              {
                this.lab_filter.push(this.labs[j]);
              }
            }
            
          }
        }
        console.log(this.lab_filter);
      }
      //----------------------------------------------------------------------------------
      //Filtro por nome do Laboratório
      if(this.isLab)
      {
        console.log("labF");
        let results = this.labs.filter((lab) => {
          return (lab.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
        console.log(results);
        for(let result of results)
        {
          this.lab_filter.push(result);
        }  
      }
      console.log(this.lab_filter.length);
      let count = 0;
      for(let i = 0; i < this.lab_filter.length; i++)
      {
        count = 0;
        console.log(i);
        for(let j = (i+1); j < this.lab_filter.length; j++)
        {
          //console.log(j);
          if(this.lab_filter[i].idlaboratorio == this.lab_filter[j].idlaboratorio)
          { 
            count = 1;
            this.labs = [];
            console.log(i, j, this.lab_filter[i].idlaboratorio, this.lab_filter[j].idlaboratorio);
            break;
          }
        }
        if(count == 0)
        {
          //this.techs = [];
          labs2.push(this.lab_filter[i]);
          append = 1;
          console.log("entre count 0");
        }
      }
      console.log(this.lab_filter);
      console.log(labs2);
      if(append == 0)
      {
        this.labs = this.lab_filter;
        console.log("aqui");
      }
      else
      {
        this.labs = labs2;
        console.log("caralho");
      }
    }
  }

  
  public goTecnologyProject():void{
    this.navCtrl.push(TecnologyProject);
  }

  public goServices():void{
    this.navCtrl.push(ServiceSearchPage);
  }

  public goProjects():void{
    this.navCtrl.push(Projects);    
  }

  public goToLogin():void{
    this.navCtrl.push(LoginPage);    
  }

  private goToDesc(tech:any):void{
    this.navCtrl.push(TecnologyDescPage, {tech:tech});
  }

}
