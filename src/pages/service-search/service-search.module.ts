import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServiceSearchPage } from './service-search';

@NgModule({
  declarations: [
    ServiceSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(ServiceSearchPage),
  ],
  exports: [
    ServiceSearchPage
  ]
})
export class ServiceSearchPageModule {}
