import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Services } from '../services/services';
import { TecnologyProject } from '../tecnology-project/tecnology-project';
import { LoginPage } from '../login/login'


@IonicPage()
@Component({
  selector: 'page-projects',
  templateUrl: 'projects.html',
})
export class Projects {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {
  }
  
  goServices():void{
    this.navCtrl.push(Services);
  }

  goTecnologyProject():void{
    this.navCtrl.push(TecnologyProject);
  }

  goProjects():void{
    this.navCtrl.push(Projects);
  }

  goToLogin():void{
    this.navCtrl.push(LoginPage);
  }
}
