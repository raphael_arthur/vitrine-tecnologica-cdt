import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Projects } from './projects';

@NgModule({
  declarations: [
    Projects,
  ],
  imports: [
    IonicPageModule.forChild(Projects),
  ],
  exports: [
    Projects
  ]
})
export class ProjectsModule {}
