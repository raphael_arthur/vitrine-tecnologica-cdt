import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InventorService } from '../../services/inventor.service';
import { LabService } from '../../services/laboratory.service';
import { EquipmentService } from '../../services/equipment.service';

import { Service } from '../../models/services';
/**
 * Generated class for the ServiceDescPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-service-desc',
  templateUrl: 'service-desc.html',
})
export class ServiceDescPage {

  private teacher_observer:any;
  private department_observer:any;
  private key_observer:any;
  private service_observer:any;
  private equip_observer:any;

  private services:Array<Service>;

  private has_teacher:any;
  private has_dep:any;
  private has_key:any;
  private has_service:any;
  private has_lab:any;
  private has_equip:any;

  private lab:any;

  private lab_img:any;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private teacherService:InventorService,
              private labService:LabService,
              private equipService:EquipmentService,
            ) 
  {
    this.lab = this.navParams.get('lab');
    this.services = [];
    console.log(this.lab);

    this.getLab_has_teacher(this.lab.idlaboratorio);
    this.getLab_has_dep(this.lab.idlaboratorio);
    this.getLab_has_key(this.lab.idlaboratorio);
    this.getLab_has_service(this.lab.idlaboratorio);
    this.getLab_has_equip(this.lab.idlaboratorio);
    this.lab_img = 
    [
      "assets/img/got.jpg",
      "assets/img/tecnology.png",
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServiceDescPage');
  }

  private getLab_has_teacher(id:string):void
  {
    this.teacher_observer = this.labService.getLab_has_inventor(id).subscribe( data => {
        this.has_teacher = this.labService.has_inventor;
        //this.GetTeacherNames();
    });
  }

  private getLab_has_dep(id:string):void
  {
    this.department_observer = this.labService.getLab_has_dep(id).subscribe( data => {
        console.log("dentro do subscribe");
        this.has_dep = this.labService.has_dep;
        console.log(this.has_dep[0]);
    });
  }

  private getLab_has_key(id:string):void
  {
    this.key_observer = this.labService.getLab_has_key(id).subscribe( data => {
        this.has_key = this.labService.has_key;
    });
  }


  private getLab_has_service(id:string):void
  {
    this.service_observer = this.labService.getLab_has_service(id).subscribe( data => {
        this.has_service = this.labService.has_service;
        this.concat_service();
    });
  }

  private getLab_has_equip(id:string):void
  {
    this.equip_observer = this.equipService.getLab(id).subscribe( data => {
        this.has_equip = this.equipService.equipment_found;
    });
  }

  private concat_service():void
  {
    if(this.has_service != "")
    {
      for(let service of this.has_service)
      {
        let temp = new Service();
        if(this.services.length == 0)
        {
          temp.name = service.servico;
          temp.subname[0] = service.subservico;
          this.services.push(temp);
          console.log("caralho");
        }
        else
        {
          let flag = 0; //Flag para adicionar serviços no vetor;
          for(let i = 0; i < this.services.length; i++)
          {
            if(this.services[i].name == service.servico)
            {
              for(let j = 0; j < this.services[i].subname.length; j++)
              {
                if(this.services[i].subname[j] != service.subservico)
                {
                  let temp2 = service.subservico;
                  this.services[i].subname.push(temp2);
                  console.log("porra");
                }
              }
              flag = 0;
            }
            else
            {
              flag = 1;
            }
          }
          if(flag)
          {
            temp = new Service();
            temp.name = service.servico;
            temp.subname[0] = service.subservico;
            this.services.push(temp);
            console.log("vaaaaaai");
          }
        }

        
        /*for(let sub of service.subservico)
        this.equips.push(equip.equipamento);*/
        console.log(service);
      }
    }
  }
}
