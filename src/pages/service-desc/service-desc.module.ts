import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServiceDescPage } from './service-desc';

@NgModule({
  declarations: [
    ServiceDescPage,
  ],
  imports: [
    IonicPageModule.forChild(ServiceDescPage),
  ],
  exports: [
    ServiceDescPage
  ]
})
export class ServiceDescPageModule {}
