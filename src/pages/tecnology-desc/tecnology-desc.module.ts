import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TecnologyDescPage } from './tecnology-desc';

@NgModule({
  declarations: [
    TecnologyDescPage,
  ],
  imports: [
    IonicPageModule.forChild(TecnologyDescPage),
  ],
  exports: [
    TecnologyDescPage
  ]
})
export class TecnologyDescPageModule {}
