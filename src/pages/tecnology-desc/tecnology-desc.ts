import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InventorService } from '../../services/inventor.service';
import { DepartmentService } from '../../services/department.service';
import { KeyService } from '../../services/key.service';
import { TechService } from '../../services/tecnology.service';
import { Projects } from '../projects/projects';
import { Services } from '../services/services';
import { TecnologyProject } from '../tecnology-project/tecnology-project';
import { LoginPage } from '../login/login';
import { FileUploadService } from '../../services/UploadFileService';


@IonicPage()
@Component({
  selector: 'page-tecnology-desc',
  templateUrl: 'tecnology-desc.html',
})
export class TecnologyDescPage {

  private tech:any;
  
  private teacher_observer:any;
  private teachers:any;
  private has_teacher:any;
  private teachersData:any;

  private teacherImage:any;
  private temporaryImage:any;

  private department_observer:any;
  private departments:any;
  private has_dep:any;


  private key_observer:any;
  private keys:any;
  private has_key:any;
  private keyNames:any;

  private imageFormat:string;

  private defaultImage:boolean;

  constructor(
                public navCtrl: NavController,
                public navParams: NavParams,
                private techService:TechService,
                private teacherService:InventorService,
                private departmentService: DepartmentService,
                private keyService: KeyService,
                private fileUploadService:FileUploadService
              )
    {
    this.tech = this.navParams.get('tech');
    this.keyNames = [];
    this.teachersData = [];

    this.getTeachers();
    this.getDepartments();
    this.getKeys();

    this.getTec_has_dep(this.tech.idtecnologia);
    this.getTec_has_inventor(this.tech.idtecnologia);
    this.getTec_has_key(this.tech.idtecnologia);
  }

  private getTeachers():void{
    this.teacher_observer = this.teacherService.getAllInventores().subscribe( data => {
        this.teachers = this.teacherService.inventor_found;
    });
  }

  private getDepartments():void{
    this.department_observer = this.departmentService.getAllDepartments().subscribe( data => {
        this.departments = this.departmentService.department_found;
    });
  }

  private getKeys():void{
    this.key_observer = this.keyService.getAllKeys().subscribe( data => {
        this.keys = this.keyService.key_found;
    });
  }

  private getTec_has_dep(id:string):void{
    this.department_observer = this.techService.getTec_has_dep(id).subscribe( data => {
        this.has_dep = this.techService.has_dep;
        console.log(this.has_dep[0]);
    });
  }

  private getTec_has_inventor(id:string):void{
    this.department_observer = this.techService.getTec_has_inventor(id).subscribe( data => {
        this.has_teacher = this.techService.has_inventor;
        if(this.has_teacher){
          console.log(this.has_teacher);
          this.GetTeacherNames();
        }
    });
  }

  private getTec_has_key(id:string):void{
    this.department_observer = this.techService.getTec_has_key(id).subscribe( data => {
        this.has_key = this.techService.has_key;
        if(this.has_key){
          console.log(this.has_key);
          this.GetKeyNames();
        }
      });
  }

  /*
  Guilherme Natal 07/03/2018
  A função GetKeyNames usa os ids encontrados em getTech_has_key
  para ir na tabela de key-word e pegar os nomes dessas chaves. 
  */

  public GetKeyNames():void{
    for(let techKeys of this.has_key){
      for(let key of this.keys){
        if(key.idpalavra_chave == techKeys.palavra_chave_idpalavra_chave){
          this.keyNames.push(key.nome);
        }
      }
    }
  }


  /*
  Guilherme Natal 07/03/2018
  A função GetKeyNames usa os ids encontrados em getTech_has_key
  para ir na tabela de key-word e pegar os nomes dessas chaves. 
  */

  public GetTeacherNames():void{
    for(let techTeachers of this.has_teacher){
      for(let teacher of this.teachers)
        if(teacher.idinventor == techTeachers.inventor_idinventor){
          this.fileUploadService
          .GetImage('http://localhost:8080/server_vitrine/request/inventor_getFiles.php',teacher.idinventor)
          .subscribe( data => {
            if(data['_body'] != "noImage"){
              this.temporaryImage = JSON.parse(data['_body']);
              this.imageFormat = this.GetImageFormat(this.temporaryImage[0].imageName);
              this.teachersData.push({nome:teacher.nome,lattes:teacher.lattes,image:this.temporaryImage[0].image});
             }
             else{
              this.teachersData.push({nome:teacher.nome,lattes:teacher.lattes});               
              this.defaultImage = true;
             }
            }
          );

        }
    }  
  }

  private GetImageFormat(imageName:string):string{
    let dotIndex = imageName.lastIndexOf(".");
    let length =  imageName.length;
    let imageType =  imageName.substring(dotIndex+1,length);
    return  imageType;
  }

  goToLogin():void{
    this.navCtrl.push(LoginPage);
  }

  goProjects():void{
    this.navCtrl.push(Projects);
  }

  goServices():void{
    this.navCtrl.push(Services);
  }

  goTecnologyProject():void{
    this.navCtrl.push(TecnologyProject);
  }
}
