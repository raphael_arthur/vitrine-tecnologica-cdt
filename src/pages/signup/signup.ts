import { Component } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { UserService } from '../../services/user.service';
import { SetorService } from '../../services/setor.service';

@Component({
    selector: 'sign-up-page',
    templateUrl: 'signup.html'
})
export class SignUpPage{
    private signup:FormGroup;
    private setor_observer:any;
    private setores:any;
    private testes:any;

    constructor(
        private formBuilder: FormBuilder,
        private userService:UserService, 
        private setorService:SetorService,
        private alertController:AlertController
     )
     {
        this.getAllSetores();
        this.testes = [0,1];

        this.signup = this.formBuilder.group({
            firstName: [null, Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*')])],
            lastName: [null, Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*')])],
            email:[null, Validators.required],
            password: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
            ramal: [null, Validators.compose([Validators.required, Validators.pattern('[0-9 ]*')])],
            setor: [null, Validators.required],
            userLvl: [null, Validators.required],
            passwordValidation: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
            
        });
        
            console.log("estou signup")
            console.log(this.setores);
    }


    public getAllSetores():void
    {
        this.setor_observer = this.setorService.getAllSetores().subscribe( data => {
            console.log("dentro do subscribe");
            this.setores = this.setorService.setor_found;
            console.log(this.setores);
        });
    }
    
    public sendData(){
        if(this.signup.value.password == this.signup.value.passwordValidation){
            this.userService.createUser(this.signup.value).subscribe(data =>{
                console.log(data);
            });
            let alert =  this.alertController.create({
                title:"usuario criado",
                message:"usuario criado com sucesso",
                buttons:[{
                    text:"ok"
                }]
            });
            alert.present();
        }
        else{
            let alert =  this.alertController.create({
                title:"senhas diferentes",
                message:"inserir senhas iguais por favor",
                buttons:[{
                    text:"ok"
                }]
            });
            alert.present();
        }
    }
}

