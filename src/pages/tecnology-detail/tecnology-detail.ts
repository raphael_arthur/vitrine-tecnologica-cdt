import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//-----------------Service------------------------------------
import { TechService } from '../../services/tecnology.service';

//-----------------Pages-------------------------------------
import { TecnologyDescPage } from '../tecnology-desc/tecnology-desc';
import { Projects } from '../projects/projects';
import { Services } from '../services/services';
import { TecnologyProject } from '../tecnology-project/tecnology-project';
import { LoginPage } from '../login/login';

@IonicPage()
@Component({
  selector: 'page-tecnology-detail',
  templateUrl: 'tecnology-detail.html',
})
export class TecnologyDetail {

  private tech_observer:any;
  private techs:any;

  private keys: any;
  private departments: any;

  private id_ext:any; //id_ext = parametro de busca da página inicial
  private searchQuery: string = '';
  private techs_bkp:any;
  private isName:boolean;
  private isCompleteName:boolean;
  private isDataProtect: boolean;
  private isInventor: boolean;
  private isKey: boolean;
  private isDepartment: boolean; 
  private tech_filter:any;

  private filter_inventor:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public techService: TechService) 
  {
    this.isName = true;
    this.isCompleteName = false;
    this.isDataProtect = false;
    this.isInventor = false;
    this.isKey = false;
    this.isDepartment = false;

    this.search_by_inventor();
    this.search_by_key();
    this.search_by_department();

    this.id_ext = this.navParams.get('id');
    console.log(this.id_ext);
    if(this.id_ext == null)
      this.getTechs();
    else
      this.search_by_category(this.id_ext);
    
    console.log(this.isName);
  }

  private getTechs():void{
    this.tech_observer = this.techService.getAllTechs().subscribe( data => {
        this.techs = this.techService.tech_found;
        this.techs_bkp = this.techs;
    });
  }

  private goToDesc(tech:any):void{
    this.navCtrl.push(TecnologyDescPage, {tech:tech});
  }

  private search_by_category(id:any):void{
    console.log(id);
    this.tech_observer = this.techService.search_by_category(id).subscribe( data => {
      this.techs = this.techService.tec_found_by_category;
      this.techs_bkp = this.techs;
    });
  }

  private search_by_inventor():void{
    this.tech_observer = this.techService.search_by_inventor().subscribe( data => {
      this.filter_inventor = this.techService.tech_found;
    });
  }

  private search_by_key():void{
    this.tech_observer = this.techService.search_by_key().subscribe( data => {
      this.keys = this.techService.tech_found;
    });
  }

  private search_by_department(){
    this.tech_observer = this.techService.search_by_department().subscribe( data => {
      this.departments = this.techService.tech_found;
    });
  }

  private getItems(ev: any):void{
    console.log(this.techs);
    this.techs = this.techs_bkp;
    let val = ev.target.value;
    let techs2 = [];
    let append = 0;
    
    console.log(this.isName);
    
    console.log(this.isInventor);
    console.log(this.isKey);
    console.log(this.isDepartment);
    this.tech_filter = [];

    if (val && val.trim()!= ''){
      this.FilterByInventor(val);
      this.FilterByKey(val);
      this.FilterByDepartment(val);
      this.FilterByTechnicalName(val);
      this.FilterByDataProtected(val);
      this.FilterByName(val);

      console.log(this.tech_filter.length);
      let count = 0;
      for(let i = 0; i < this.tech_filter.length; i++){
        count = 0;
        console.log(i);
        for(let j = (i+1); j < this.tech_filter.length; j++){
          if(this.tech_filter[i].idtecnologia == this.tech_filter[j].idtecnologia){ 
            count = 1;
            this.techs = [];
            console.log(i, j, this.tech_filter[i].idtecnologia, this.tech_filter[j].idtecnologia);
            break;
          }
        }
        if(count == 0){
          techs2.push(this.tech_filter[i]);
          append = 1;
          console.log("entre count 0");
        }
      }
      console.log(this.tech_filter);
      console.log(techs2);
      if(append == 0){
        this.techs = this.tech_filter;
        console.log("aqui");
      }
      else{
        this.techs = techs2;
        console.log("caralho");
      }
    }
  }

  public FilterByKey(val:any):void{
    if(this.isKey){
      console.log(this.keys);
      let results = this.keys.filter((key) => {
        return (key.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
      console.log(results);
      if(results.length > 0)
      {
        for(let i = 0; i < results.length; i++)
        {
          for(let j = 0; j < this.techs.length; j++)
          {
            if(results[i].tecnologia_idtecnologia == this.techs[j].idtecnologia)
            {
              this.tech_filter.push(this.techs[j]);
            }
          }
          
        }
      }
      console.log(this.tech_filter);
    }

  }

  public FilterByDataProtected(val:any):void{
    if(this.isDataProtect){
      console.log("DP");
      let results = this.techs.filter((tech) => {
        return (tech.dados_protecao.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
      console.log(results);
      for(let result of results)
      {
        this.tech_filter.push(result);
      }
    }
  }

  public FilterByDepartment(val:any):void{
    if(this.isDepartment){
      console.log(this.departments);
      let results = this.departments.filter((department) => {
        return (department.nome.toLowerCase().indexOf(val.toLowerCase()) > -1 || department.sigla.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
      console.log(results);
      if(results.length > 0)
      {
        for(let i = 0; i < results.length; i++)
        {
          for(let j = 0; j < this.techs.length; j++)
          {
            if(results[i].tecnologia_idtecnologia == this.techs[j].idtecnologia)
            {
              this.tech_filter.push(this.techs[j]);
            }
          }
          
        }
      }
      console.log(this.tech_filter);
    }
  }

  public FilterByInventor(val:any):void{
    if(this.isInventor){
      console.log(this.filter_inventor);
      console.log(val);
      let results = this.filter_inventor.filter((inventor) => {
        return (inventor.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
      console.log(results);
      if(results.length > 0){
        for(let i = 0; i < results.length; i++){
          for(let j = 0; j < this.techs.length; j++){
            if(results[i].tecnologia_idtecnologia == this.techs[j].idtecnologia){
              this.tech_filter.push(this.techs[j]);
            }
          }          
        }
      }
      console.log(this.tech_filter);
    }
  }

  public FilterByTechnicalName(val:any):void{
    if(this.isCompleteName)
    {
      console.log("cn");
      let results = this.techs.filter((tech) => {
        return (tech.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
      console.log(results);
      for(let result of results)
      {
        this.tech_filter.push(result);
      }  
    }
  }

  public FilterByName(val:any):void{
    
    if(this.isName){
      console.log("n");
      let results = this.techs.filter((tech) => {
        return (tech.apelido.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
      console.log(results);
      for(let result of results){
        this.tech_filter.push(result);
      }
    }

  }

  goProjects():void{
    this.navCtrl.push(Projects);
  }

  goServices():void{
    this.navCtrl.push(Services);
  }

  goTecnologyProject():void{
    this.navCtrl.push(TecnologyProject);
  }

  goToLogin():void{
    this.navCtrl.push(LoginPage);
  }
}
