import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TecnologyDetail } from './tecnology-detail';

@NgModule({
  declarations: [
    TecnologyDetail,
  ],
  imports: [
    IonicPageModule.forChild(TecnologyDetail),
  ],
  exports: [
    TecnologyDetail
  ]
})
export class TecnologyDetailModule {}
