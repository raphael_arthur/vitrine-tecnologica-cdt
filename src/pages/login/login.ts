import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from '../../app/core/services/auth.service';
import { AlertController } from 'ionic-angular';
import { NavController, NavParams } from 'ionic-angular';
import { EditPage } from '../edit/edit';
import { RetrivePasswordPage } from '../retrive-password/retrive-password';

@Component({
    selector: 'login-page',
    templateUrl: 'login.html'
})
export class LoginPage{

    private login : FormGroup;
    public user: any;
    public password:any;
    public keepConnected: boolean;

    constructor( private alertCtrl:AlertController, private formBuilder: FormBuilder, private auth: AuthService, public navCtrl: NavController, public navParams: NavParams  /*private router: Router*/){
        //construct the FormBuilder to login forms
        this.login = this.formBuilder.group({
            email: [null, Validators.required],
            password: [null, Validators.required],
        });
        this.user = this.getCookie("name");
        this.password = this.getCookie("pass");
        if(this.user){
            this.ByPassLogin();
        }
        this.keepConnected = false;
    }

    public BackToInitialPage():void{
        this.deleteAllCookies();
        location.reload();
    }       

    public deleteAllCookies() {
        var cookies = document.cookie.split(";");
    
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            var eqPos = cookie.indexOf("=");
            var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
    }
    

    public getCookie(name: string): string {
        const nameLenPlus = (name.length + 1);
        return document.cookie
            .split(';')
            .map(c => c.trim())
            .filter(cookie => {
                return cookie.substring(0, nameLenPlus) === `${name}=`;
            })
            .map(cookie => {
                return decodeURIComponent(cookie.substring(nameLenPlus));
            })[0] || null;
    }

    public ByPassLogin(){
        this.auth.login(this.user, this.password).subscribe(data => {
            if(sessionStorage.getItem('userBlocked') == 'true'){
                this.presentAlert();
                //this.router.navigate(['/home']);
            } else if(sessionStorage.getItem('userToken') != "" ){ //mudar para !=  "" esse == é para teste apenas
                console.log("TENHO TOKEN");
                this.navCtrl.push(EditPage);
            }
            console.log(data);
        });
    }

    public sendData():void{
        if(this.keepConnected){
            document.cookie = 'name='+this.login.value.email;
            document.cookie = 'pass='+this.login.value.password;
        }
        this.auth.login(this.login.value.email, this.login.value.password).subscribe(data => {
            let token = sessionStorage.getItem('userToken');
            console.log(sessionStorage);
            console.log(token);
            if(sessionStorage.getItem('userBlocked') == 'true'){
                this.presentAlert();
                //this.router.navigate(['/home']);
            } else if(sessionStorage.getItem('userToken') != "" ){ //mudar para !=  "" esse == é para teste apenas
                console.log("TENHO TOKEN");
                this.navCtrl.push(EditPage);
            }
            console.log(data);
        });
    }

    private presentAlert():void {
        let alert = this.alertCtrl.create({
            title: 'Conta Bloqueada',
            subTitle: 'Por questões de segurança sua conta foi bloqueada. Por favor, envie um email para comunicacao@cdt.unb.br.',
            buttons: ['Entendi']
        });
        alert.present();
    }

    /*
    guilherme natal 28/02/2018
    A função KeepMelogged troca o estado da variável keepConnected;
    */ 

    public KeepMeLogged():void{
        this.keepConnected = !this.keepConnected;
    }

    /*
    guilherme natal 28/02/2018
    A função PaginaRecuperacao Chama a pagina de recuperar senha

    */

    public PaginaRecuperacao():void{
        this.navCtrl.push(RetrivePasswordPage);
    }

}
