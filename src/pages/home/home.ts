import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Projects } from '../projects/projects';
import { TecnologyProject } from '../tecnology-project/tecnology-project';
import { TecnologyDetail } from '../tecnology-detail/tecnology-detail';
import { LoginPage } from '../login/login';
import { ServiceSearchPage } from '../service-search/service-search';
//services
import { CategoryService } from '../../services/category.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private category_observer:any;
  private categories:any;
  public slides_img;

  constructor(public navCtrl: NavController, private categoryService:CategoryService) 
  {
    this.getCategories();
    this.slides_img = 
    [
      "assets/img/got.jpg",
      "assets/img/tecnology.png",
    ]
  }

  private getCategories():void{
    this.category_observer = this.categoryService.getAllCategories().subscribe( data => {
        this.categories = this.categoryService.category_found;
    });
  }

  goProjects():void{
    this.navCtrl.push(Projects);
  }

  goServices(id:number):void{
    this.navCtrl.push(ServiceSearchPage, {id:id});
  }

  goTecnologyProject():void{
    this.navCtrl.push(TecnologyProject);
  }

  goTecnologyDetail(id:any):void{
    console.log(id);
    this.navCtrl.push(TecnologyDetail, {id:id});
  }

  goToLogin():void{
    this.navCtrl.push(LoginPage);
  }
}
