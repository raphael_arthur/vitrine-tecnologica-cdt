import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,Platform,ViewController} from 'ionic-angular';
import { ModalCreateTech } from './modal-create';
import { ModalEditTech } from './modal-edit';

//------Services-----------
import { TechService } from '../../services/tecnology.service';
import { InventorService } from '../../services/inventor.service';
import { DepartmentService } from '../../services/department.service';
import { KeyService } from '../../services/key.service'

@IonicPage()
@Component({
  selector: 'page-tecnology-crud',
  templateUrl: 'tecnology-crud.html',
})
export class TecnologyCrudPage {

  private tech_observer:any;
  private techs:any;
  private newTech:any;

  private teacher_observer:any;
  private teachers:any;

  private department_observer:any;
  private departments:any;

  private key_observer:any;
  private keys:any;

  constructor(
    public modalCtrl: ModalController,
    public platform: Platform,
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private techService:TechService
    ) 
  {
    this.newTech = 
    {
      'nome':  "",
      'desc': "",
      'dados_protecao': "",
      'idinventor_inventor':"",
      'idpalavra_chaveidpalavra_chave':"",
      'departamento_iddepartamento':"",
      'laboratorio_idlaboratorio':""
    };
      this.getTechs();
    }

  private getTechs():void{
    this.tech_observer = this.techService.getAllTechs().subscribe( data => {
        this.techs = this.techService.tech_found;
    });
  }

  public doRefresh(refresher) {
    this.getTechs();
    this.navCtrl.push(this.navCtrl.getActive().component).then(() => {
      let index = this.viewCtrl.index;
      this.navCtrl.remove(index);
   })
  }
  
  private del(id):void
  {
    this.techService.del(id).subscribe( data => {
        this.techs = this.techService.tech_found;
    });
    this.doRefresh(0);
  }
  
  private modalEdit(tech:any)
  {
    let modal = this.modalCtrl.create(ModalEditTech, {tech:tech});
    modal.present();
  }


  private modalCreate()
  {
    let modal = this.modalCtrl.create(ModalCreateTech);
    modal.onDidDismiss(data => {
      this.doRefresh(0);
    });
    modal.present();
  }


    /*
  
  Guilherme Natal 23/02/2018
  A função Logout desloga o usário do banco de dados
  e retorna a página inícial
  */

  public Logout():void{
    this.BackToInitialPage();
  }
  public BackToInitialPage():void{
    location.reload();
    this.DeleteAllCookies();
  }

/*
Guilherme Natal 23/02/2018
A função DeleteAllCookies deleta os cookies para que o usuário não
logue altomaticamente.
*/

  public DeleteAllCookies() {
      var cookies = document.cookie.split(";");

      for (var i = 0; i < cookies.length; i++) {
          var cookie = cookies[i];
          var eqPos = cookie.indexOf("=");
          var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
          document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
      }
  }

}
