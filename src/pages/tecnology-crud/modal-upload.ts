import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform, ViewController,AlertController } from 'ionic-angular';
import { FileChooser } from '@ionic-native/file-chooser';

import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';


@Component({
  templateUrl: 'modal-upload.html',
})


export class ModalUploadFile {


  constructor(
    public navController:NavController,
    public navParams: NavParams,
    public alert:AlertController,
  ){
  }

  
}
