import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform, ViewController, AlertController } from 'ionic-angular';

//------Services-----------
import { TechService } from '../../services/tecnology.service';
import { InventorService } from '../../services/inventor.service';
import { DepartmentService } from '../../services/department.service';
import { KeyService } from '../../services/key.service';
import { LabService } from '../../services/laboratory.service';
import {ModalCreateLab} from  '../lab-crud/modal-create'



@Component({
  selector: 'modal-edit',
  templateUrl: 'modal-edit.html',
})
export class ModalEditTech {

  private teacher_observer:any;
  private teachers:any;
  private has_teacher:any;
  private teachers_checked:any;
  private newTeacher:any;
  private inventor_id_list:string;

  private department_observer:any;
  private departments:any;
  private has_dep:any;
  private departments_checked:any;
  private department_id_list:string;


  private key_observer:any;
  private keys:any;
  private has_key:any;
  private keys_checked:any;
  private keys_id_list:string;

  private lab_observer:any;
  private labs:any;
  private has_lab:any;
  private labs_checked:any;
  private lab_id_list:string;

  private tech:any;//a variável tech possui todas as entradas de texto do usário 


  private oldDepartment:string;
  private oldInventor:string;
  private oldKey:string;
  private oldUser:string;
  private oldlab:string;


  constructor(
    public viewCtrl: ViewController, 
    public platform: Platform, 
    public navParams: NavParams, 
    private labService:LabService, 
    private techService:TechService, 
    private teacherService:InventorService, 
    private departmentService: DepartmentService, 
    private keyService: KeyService,
    private inventorService:InventorService,
    public labModal:ModalController, 
    public navCtrl: NavController,
    public inventorRegister:AlertController,
  ){
    this.newTeacher = {
      'nome':  "",
      'lattes': ""
    };

    this.has_dep = "";
    this.teachers_checked = [];
    this.labs_checked = [];
    this.departments_checked = [];
    this.keys_checked = [];

    this.tech = this.navParams.get('tech');//tech vem da página technology-crud.ts
    this.getTeachers();
    this.getDepartments();
    this.getKeys();
    this.getLabs();

    this.getTec_has_dep(this.tech.idtecnologia);
    this.getTec_has_inventor(this.tech.idtecnologia);
    this.getTec_has_key(this.tech.idtecnologia);
    this.getTec_has_lab(this.tech.idtecnologia);

    this.InitLists();
  }

  private getTeachers():void
  {
    this.teacher_observer = this.teacherService.getAllInventores().subscribe( data => {
        this.teachers = this.teacherService.inventor_found;
    });
  }

  private getLabs():void
  {
    this.lab_observer = this.labService.getAllLabs().subscribe( data => {
        this.labs = this.labService.lab_found;
    });
  }

  private getDepartments():void
  {
    this.department_observer = this.departmentService.getAllDepartments().subscribe( data => {
        this.departments = this.departmentService.department_found;
    });
  }

  private getKeys():void
  {
    this.key_observer = this.keyService.getAllKeys().subscribe( data => {
        this.keys = this.keyService.key_found;
    });
  }

  private edit(tech:any):void
  {
    console.log(this.teachers_checked);
    this.oldUser = this.tech.usuario_idusuario;
    this.techService.edit(tech, this.oldDepartment,this.department_id_list,
      this.oldInventor,
      this.inventor_id_list,
      this.oldKey,
      this.keys_id_list,
      this.oldUser,
      this.oldlab,
      this.lab_id_list
    );
    this.viewCtrl.dismiss();
  }

  /*
  guilherme natal 21/02/2018
  essa função pega todos os ids de depts do banco de dados
  e chama CheckDept e CreateDeptList
  */


  private getTec_has_dep(id:string):void{
    this.department_observer = this.techService.getTec_has_dep(id).subscribe( data => {
        this.has_dep = this.techService.has_dep;
        this.oldDepartment = this.has_dep.departamento_iddepartamento;
        this.CheckDept();
        this.CreateDeptList();
    });
  }

  /*
  guilherme natal 21/02/2018
  a função CheckDept percorre todos os ids de depts do banco de dados
  e compara com os ids de dept da tecnologia em questão para marcar no html
  */


  private CheckDept():void{
    let checked = false;

    if(this.departments != null){

      for(let j=0; j < this.departments.length; j++)
      {
        checked = false;
        for(let i = 0; i < this.has_dep.length; i++)
        {
          if(this.has_dep[i].departamento_iddepartamento == this.departments[j].iddepartamento)
          {
            let temp = [{"iddepartamento": this.departments[j].iddepartamento, "nome":this.departments[j].nome, "checked":"true"}]
            checked = true;
            this.departments_checked.push(temp[0]);
            break;
          }
        }
        if(!checked)
        {
          let temp = [{"iddepartamento": this.departments[j].iddepartamento, "nome":this.departments[j].nome, "checked":"false"}]
          this.departments_checked.push(temp[0]);
        }
      }
    }
  }

  /*
  guilherme natal 21/02/2018
  a função CreateDeptList cria uma lista separada por | de 
  ids de departamentos na tecnologia.
  */

  private CreateDeptList():void{
    for(let deptChecked of this.departments_checked){
      if(deptChecked.checked =="true")
        this.department_id_list += deptChecked.iddepartamento + "|";
    }
    console.log(this.department_id_list);
  }


  /*
  guilherme natal 21/02/2018
  a função RegisterInventor cria um inventor e adiciona ao banco de dados
  
  */

  private RegisterInventor():void{
    let controller = this.inventorRegister.create({
      title:'cadastro inventor',
      message: 'entre com o nome do do inventor',
      inputs:[{
        name:'nome',
        placeholder:'inventor'
      },
      {
        name:'lattes',
        placeholder:'Lattes(copiar e colar o link)'
      }
      ],
      buttons:[{
        text: 'confirmar',
        handler: data => {
          this.newTeacher.nome = data.nome;
          this.newTeacher.lattes = data.lattes;
          this.inventorService.create(this.newTeacher).subscribe(data => {
              console.log("inventor");
          })
        } 
      }
      ]
    });
    controller.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  /*
  a função getTec_has_inventor busca no banco de dados todos os
  ids de inventores e chama CheckInventor e CreateInventorLista  

  */

  private getTec_has_inventor(id:string):void{
    this.teacher_observer = this.techService.getTec_has_inventor(id).subscribe( data => {
        this.has_teacher = this.techService.has_inventor;
        this.oldInventor = this.has_teacher.inventor_idinventor;
        this.CheckInventor();
        this.CreateInventorList();
    });    
  }


  /*
  guilherme natal 21/02/2018
  a função checkInventor percorre toda a lista de inventores do banco de dados
  e a lista de inventores da tecnologia em questão para marcar no html
  */

  private CheckInventor():void{
    let checked = 0; 
    if(this.teachers != null){

      for(let j=0; j < this.teachers.length; j++)
      {
        checked = 0;
        for(let i = 0; i < this.has_teacher.length; i++)
        {
          if(this.has_teacher[i].inventor_idinventor == this.teachers[j].idinventor)
          {
            let temp = [{"idinventor": this.teachers[j].idinventor, "nome":this.teachers[j].nome, "checked":"true"}]
            checked = 1;
            this.teachers_checked.push(temp[0]);
            break;
          }
        }
        if(checked == 0)
        {
          let temp = [{"idinventor": this.teachers[j].idinventor, "nome":this.teachers[j].nome, "checked":"false"}]
          this.teachers_checked.push(temp[0]);
        }
      }
    }
    console.log(this.teachers_checked);

  }


  /*
  guilherme natal 21/02/2018
  a função CreateInventorList cria uma lista separada por | 
  de ids de teachers presentes na tecnologia. 
  Ex: 1|2|3|  (perceba a barra no final)
  */

  private CreateInventorList():void{
    for(let inventor_checked of this.teachers_checked){
      if(inventor_checked.checked =="true")
        this.inventor_id_list += inventor_checked.idinventor +"|";
    } 
    console.log(this.inventor_id_list);
  }

  /*
  Guilherme natal 20/02/2018
  a função abaixo recebe do banco de dados todos 
  os ids de palavras chaves existentes

  */ 

  private getTec_has_key(id:string):void{
    this.department_observer = this.techService.getTec_has_key(id).subscribe( data => {
        this.has_key = this.techService.has_key;
        this.oldKey = this.has_key.palavra_chave_idpalavra_chave;
        this.CheckTechKeys();
        this.CreateTechKeysList();
    });
  }

  /*
  Guilherme natal 20/02/2018
  a função CheckTechKeys verifica quais chaves estão
  presentes na tecnologia e coloca em aaray para ser utiliza no html
  para marcar os checkbox
  */

  private CheckTechKeys():void{
    let checked = 0;

    if(this.keys != null){

      for(let j=0; j < this.keys.length; j++)
      {
        checked = 0;
        for(let i = 0; i < this.has_key.length; i++)
        {
          if(this.has_key[i].palavra_chave_idpalavra_chave == this.keys[j].idpalavra_chave)
          {
            let temp = [{"idpalavra_chave": this.keys[j].idpalavra_chave, "nome":this.keys[j].nome, "checked":"true"}]
            checked = 1;
            this.keys_checked.push(temp[0]);
            break;
          }
        }
        if(checked == 0)
        {
          let temp = [{"idpalavra_chave": this.keys[j].idpalavra_chave, "nome":this.keys[j].nome, "checked":"false"}]
          this.keys_checked.push(temp[0]);
        }
      }
    }
  }

  /*
  guilherme natal 20/02/2018
  a função CreateTechKeyList cria uma lista separada por |
  contendo os ids de cada palavra chave para posteriormente fazer o
  update no banco de dados
  */

  private CreateTechKeysList():void{
    for(let key_check of this.keys_checked){
      if(key_check.checked == "true")
        this.keys_id_list += key_check.idpalavra_chave + "|";
    }
    console.log(this.keys_id_list);
  }

  /*
  guilherme natal 20/02/2018
  a função gettec_has_lab busca no banco de dados todos os ids
  dos laboratorios
  */

  private getTec_has_lab(id:string):void{
    this.lab_observer = this.techService.getTec_has_lab(id).subscribe( data => {
        this.has_lab = this.techService.has_lab;/*ponto de falha deixa a tela vazia caso essa tecnologia nao tenha lab*/
        console.log(this.has_lab);
        this.CheckLabs();
        this.CreateLabList();
    });
  }

  /*
  guilherme natal 20/02/2018
  a função check labs procura os ids de labs da tecnologia em questão
  e coloca em um dicionario para ser marcado nos checkbox do html
  */


  private CheckLabs():void{
    this.oldlab = this.has_lab.laboratorio_idlaboratorio;//ponto de falha (comprar um array com uma string)
    let checked = 0;
    if(this.labs != null){

      for(let j=0; j < this.labs.length; j++)
      {
        checked = 0;
        for(let i = 0; i < this.has_lab.length; i++)
        {
          if(this.has_lab[i].laboratorio_idlaboratorio == this.labs[j].idlaboratorio)
          {
            let temp = [{"idlaboratorio": this.labs[j].idlaboratorio, "nome":this.labs[j].nome, "checked":"true"}]
            checked = 1;
            this.labs_checked.push(temp[0]);
            break;
          }
        }
        if(checked == 0)
        {
          let temp = [{"idlaboratorio": this.labs[j].idlaboratorio, "nome":this.labs[j].nome, "checked":"false"}]
          this.labs_checked.push(temp[0]);
        }
      }
    }
  }

  /*
  guilherme natal 20/02/2018
  a função CreateLabList cria uma lista de ids separda por | 
  para fazer o update posteriormente no banco de dados.
  */

  private CreateLabList():void{
    for(let lab_checked of this.labs_checked){
      if(lab_checked.checked == "true")
        this.lab_id_list += lab_checked.idlaboratorio + "|";
    }    
    console.log(this.lab_id_list);

  }

  /*
  guilherme natal 21/02/2018
  a função AddRemoveDepts adiona ou remove depts
  caso o usúario click em um dept que ja esta selecionado a função o remove 
  caso o usúario selecione um dept essa função adiociona o dept
  */

  private AddRemoveDept(id:string):void{    
    if(this.department_id_list.indexOf(id) < 0){//if is not selected add at click
      this.department_id_list += id+"|";
      console.log(this.department_id_list);
    }else{//if is selected erase at click
      this.department_id_list = this.department_id_list.replace(id+"|","");
      console.log(this.department_id_list);
    }
  }


  /*
  guilherme natal 21/02/2018
  a função AddRemoveLab adiona ou remove labs
  caso o usúario click em um lab que ja esta selecionado a função o remove 
  caso o usúario selecione um lab essa função adiociona o lab
  */

  private AddRemoveLab(id:string){
    if(this.lab_id_list.indexOf(id) < 0){
      this.lab_id_list += id + "|";
      console.log(this.lab_id_list);
    }else{
      this.lab_id_list =  this.lab_id_list.replace(id + "|","");
      console.log(this.lab_id_list);
    }
  }

  /*
  guilherme natal 21/02/2018
  a função AddRemoveKeyWord adiona ou remove labs
  caso o usúario click em um key word que já esta selecionado a função o remove 
  caso o usúario selecione um key word essa função adiociona o key word
  */

  private AddRemoveKeyWord(id:string):void {
    if(this.keys_id_list.indexOf(id) < 0){
        this.keys_id_list += id+"|";
        console.log(this.keys_id_list);
    }else{
      this.keys_id_list = this.keys_id_list.replace(id + "|","");
      console.log(this.keys_id_list);
    }
  }

  private AddRemoveInventor(id:string):void{
    if(this.inventor_id_list.indexOf(id) < 0){
      this.inventor_id_list += id + "|";
      console.log(this.inventor_id_list + "Added");
    }else{
      this.inventor_id_list =  this.inventor_id_list.replace(id+"|","");
      console.log(this.inventor_id_list + "removed");
    }

  }

  /*
  guilherme natal 21/02/2018
  A função InitLists inícia as listas com strings vazias
  caso vá criar qualquer lista por favor inicie ela nesta função(grato).
  */

  private InitLists():void{
    this.lab_id_list = "";
    this.keys_id_list = "";
    this.inventor_id_list = "";
    this.department_id_list = "";
  }

  /*
  guilherme natal 22/02/2018
  a função RegisterLab chama o página de criar um lab e da refresh na página
  */

  private RegisterLab():void{
    let modal = this.labModal.create(ModalCreateLab);
    modal.onDidDismiss(data => {
      this.viewCtrl.dismiss();
    });
    modal.present();
  }
}