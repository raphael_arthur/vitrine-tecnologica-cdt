import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform, ViewController,AlertController } from 'ionic-angular';


import {ModalCreateLab} from  '../lab-crud/modal-create'
import {ModalCreateKeyWord} from  '../key-word-crud/modal-create'

//------Services-----------
import { TechService } from '../../services/tecnology.service';
import { InventorService } from '../../services/inventor.service';
import { DepartmentService } from '../../services/department.service';
import { KeyService } from '../../services/key.service';
import { LabService } from '../../services/laboratory.service';
import { FileUploadService } from '../../services/UploadFileService';


@Component({
  selector: 'modal-create',
  templateUrl: 'modal-create.html',
})
export class ModalCreateTech {

  private tech_observer:any;
  private techs:any;
  private newTech:any;
  public last_tech_id:any;

  private teacher_observer:any;
  private teachers:any;
  private newTeacher:any;

  private department_observer:any;
  private departments:any;

  private key_observer:any;
  private keys:any;

  private lab_observer:any;
  private labs:any;

  public fileToUpload: File = null;

  constructor(
    public viewCtrl: ViewController,
    public platform: Platform,
    public navParams: NavParams,
    private labService:LabService,
    private techService:TechService,
    private inventorService:InventorService,
    private teacherService:InventorService,
    private departmentService: DepartmentService,
    private keyService: KeyService,
    public labRegister: AlertController,
    public inventorRegister:AlertController,
    public uploadFileAlert:AlertController,
    public webUploader: ModalController,
    public keyWordRegister:ModalController,
    public labModal:ModalController, 
    public navCtrl: NavController,
    public uploadService:FileUploadService
    )   
  {
    this.newTech = 
    {
      'nome':  "",
      'desc': "",
      'dados_protecao': "",
      'idinventor': "",
      'iddepartamento': "",
      'idpalavra_chave': "",
      'idlab':"",
      'apelido':"",
      'situacao':"",
      'resumo':"",
      'vantagem':"",
      'licenciada':"",
      'fileName':""
    };

    this.newTeacher = 
    {
      'nome':  "",
      'lattes': ""
    };

    this.getTeachers();
    this.getDepartments();
    this.getKeys();
    this.getLabs();

  }

  private getTeachers():void{
    this.teacher_observer = this.teacherService.getAllInventores().subscribe( data => {
        console.log("dentro do subscribe");
        this.teachers = this.teacherService.inventor_found;
    });
  }

  private getDepartments():void{
    this.department_observer = this.departmentService.getAllDepartments().subscribe( data => {
        console.log("dentro do subscribe");
        this.departments = this.departmentService.department_found;
    });
  }

  private getLabs():void
  {
    this.lab_observer = this.labService.getAllLabs().subscribe( data => {
        console.log("dentro do subscribe");
        this.labs = this.labService.lab_found;
    });
  }

  private getKeys():void
  {
    this.key_observer = this.keyService.getAllKeys().subscribe( data => {
        console.log("dentro do subscribe");
        this.keys = this.keyService.key_found;
    });
  }


    /*
  Guilherme Natal 13/03/2018
  A função HandleFileInput pega a o arquivo escolhido do formulario
  e coloca em fileToUpload
  */

  public HandleFileInput(files: FileList) {
    this.fileToUpload = document.forms['formImage']['file'].files[0];
  }

  private create(tech:any):void
  {
    let body = new FormData();
    let hasFile:boolean = false;
    if(this.fileToUpload){
      hasFile = true;
      body.append('file',this.fileToUpload,this.fileToUpload.name);
    }
    if(hasFile){
      tech.fileName = this.fileToUpload.name;
      this.uploadService.PostFile(body,'http://localhost:8080/server_vitrine/request/upload_file.php').subscribe(data => {
        console.log(data);
      }, error => {
        console.log(error);
      });
    }

    this.techService.create(tech).subscribe( data => 
    {
        console.log(data);
    });

    this.dismiss();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  private registerLab():void{
        let modal = this.labModal.create(ModalCreateLab);
        modal.present();
}

  private AddDepartamento(departamento:any):void{
    if(this.newTech.iddepartamento.indexOf(departamento) < 0){
      this.newTech.iddepartamento += departamento+"|";
      console.log(this.newTech.iddepartamento);
    }else{
      this.newTech.iddepartamento = this.newTech.iddepartamento.replace(departamento+"|","");
      console.log(this.newTech.iddepartamento);
    }
  }

  private AddLab(lab:any):void{
    if(this.newTech.idlab.indexOf(lab) < 0){//serach for lab at the string
      this.newTech.idlab += lab+"|";
      console.log(this.newTech.idlab);
    }else{//if the lab is present on the string erase it
      this.newTech.idlab = this.newTech.idlab.replace(lab+"|","");
      console.log(this.newTech.idlab);
    }
  } 

  private AddInventor(inventor:any):void{
    if(this.newTech.idinventor.indexOf(inventor) < 0){//if is not selected add at click
      this.newTech.idinventor += inventor+"|";
      console.log(this.newTech.idinventor);
    }else{//if is selected erase at click
      this.newTech.idinventor = this.newTech.idinventor.replace(inventor+"|","");
      console.log(this.newTech.idinventor);
    }
  }

  private AddKeyWord(keyWord:any):void{
    if(this.newTech.idpalavra_chave.indexOf(keyWord) < 0){//if is not selected add at click
      this.newTech.idpalavra_chave += keyWord+"|";
      console.log(this.newTech.idpalavra_chave);
    }else{//if is selected erase at click
      this.newTech.idpalavra_chave = this.newTech.idpalavra_chave.replace(keyWord+"|","");
      console.log(this.newTech.idpalavra_chave);
    }
  }
  
  private registerInventor():void{
    let controller = this.inventorRegister.create({
      title:'cadastro inventor',
      message: 'entre com o nome do do inventor',
      inputs:[{
        name:'nome',
        placeholder:'inventor'
      },
      {
        name:'lattes',
        placeholder:'Lattes(copiar e colar o link)'
      }
      ],
      buttons:[{
        text: 'confirmar',
        handler: data => {
          this.newTeacher.nome = data.nome;
          this.newTeacher.lattes = data.lattes;
          this.teacherService.create(this.newTeacher).subscribe(data => {
              console.log("inventor");
          })
        } 
      }
      ]
    });
    controller.present();
  }

  public ShowKeyWordRegister():void{
    let modal =  this.keyWordRegister.create(ModalCreateKeyWord);
    modal.present();    
  }


}

