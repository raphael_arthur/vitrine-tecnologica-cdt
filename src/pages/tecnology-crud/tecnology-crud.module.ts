import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TecnologyCrudPage } from './tecnology-crud';

@NgModule({
  declarations: [
    TecnologyCrudPage,
  ],
  imports: [
    IonicPageModule.forChild(TecnologyCrudPage),
  ],
  exports: [
    TecnologyCrudPage
  ]
})
export class TecnologyCrudPageModule {}
