import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { SetorService } from '../../services/setor.service';

@IonicPage()
@Component({
  selector: 'page-sector-crud',
  templateUrl: 'sector-crud.html',
})
export class SectorCrudPage {

  private setor_observer:any;
  private setores:any;
  private newSetor:string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private setorService:SetorService, 
    private formBuilder:FormBuilder
  ) 
  {
    this.newSetor = "";
    this.getCategories();
  }

  public doRefresh(refresher) {
    this.getCategories();
    this.navCtrl.push(this.navCtrl.getActive().component).then(() => {
      let index = this.viewCtrl.index;
      this.navCtrl.remove(index);
   });
  }

  private getCategories():void{
    this.setor_observer = this.setorService.getAllSetores().subscribe( data => {
        console.log("dentro do subscribe");
        this.setores = this.setorService.setor_found;
    });
  }

  private del(id):void{
    this.setorService.del(id).subscribe( data => {
        console.log("dentro do subscribe");
        this.setores = this.setorService.setor_found;
    });
    this.doRefresh(0);
  }
  
  private edit(setor:any):void{
    this.setorService.edit(setor);
    this.doRefresh(0);
  }

  private create(setor:any):void{
    console.log(this.newSetor);
    this.setorService.create(setor);
    this.doRefresh(0);
  }
  /*
  
  Guilherme Natal 23/02/2018
  A função Logout desloga o usário do banco de dados
  e retorna a página inícial
  */

  public Logout():void{
    this.BackToInitialPage();
  }
  public BackToInitialPage():void{
    location.reload();
    this.DeleteAllCookies();
  }

/*
Guilherme Natal 23/02/2018
A função DeleteAllCookies deleta os cookies para que o usuário não
logue altomaticamente.
*/

  public DeleteAllCookies() {
      var cookies = document.cookie.split(";");

      for (var i = 0; i < cookies.length; i++) {
          var cookie = cookies[i];
          var eqPos = cookie.indexOf("=");
          var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
          document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
      }
  }

}
