import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SectorCrudPage } from './sector-crud';

@NgModule({
  declarations: [
    SectorCrudPage,
  ],
  imports: [
    IonicPageModule.forChild(SectorCrudPage),
  ],
  exports: [
    SectorCrudPage
  ]
})
export class SectorCrudPageModule {}
