import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserService } from '../../services/user.service';

//------------------------Pages-------------------------------
import { SignUpPage } from '../signup/signup';
import { UserEditPage } from '../user-edit/user-edit';
import { DepartmentCrudPage } from '../department-crud/department-crud';
import { CategoryCrudPage } from '../category-crud/category-crud';
import { KeyWordCrudPage } from  '../key-word-crud/key-word-crud';
import { SectorCrudPage } from  '../sector-crud/sector-crud';
import { TeacherCrudPage } from '../teacher-crud/teacher-crud';
import { TecnologyCrudPage } from '../tecnology-crud/tecnology-crud';
import { LabCrudPage } from '../lab-crud/lab-crud';
import { EquipmentsPage } from '../equipments/equipments';
import { ServicesCrudPage } from '../services-crud/services-crud';

@IonicPage()
@Component({
  selector: 'page-edit',
  templateUrl: 'edit.html',
})
export class EditPage {


  private userID:any;
  private userData:any;
  private user_observer:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams, 
    public userService: UserService
  ){
    this.userID = sessionStorage.getItem('userID');
    this.getUser(this.userID);
  }

  goToCreateUser():void{
    this.navCtrl.push(SignUpPage);
  }

  goToEditUser():void{
    this.navCtrl.push(UserEditPage, {userData:this.userData});
  }

  goToDepartment():void{
    this.navCtrl.push(DepartmentCrudPage, {userData:this.userData});
  }

  goToCategory():void{
    this.navCtrl.push(CategoryCrudPage, {userData:this.userData});
  }

  goToKeyWord():void{
    this.navCtrl.push(KeyWordCrudPage,{userData:this.userData});
  }

  goToSector():void{
    this.navCtrl.push(SectorCrudPage,{userData:this.userData});
  }

  goToTeacher():void{
    this.navCtrl.push(TeacherCrudPage,{userData:this.userData});
  }

  goToTech():void{
    this.navCtrl.push(TecnologyCrudPage,{userData:this.userData});
  }

  goToLab():void{
    this.navCtrl.push(LabCrudPage,{userData:this.userData});
  }

  goToEquipment():void{
    this.navCtrl.push(EquipmentsPage,{userData:this.userData});
  }

  goToService():void{
    this.navCtrl.push(ServicesCrudPage,{userData:this.userData});
  }

  getUser(userID):any{
    this.user_observer = this.userService.getUser(userID).subscribe( data => {
        this.userData = this.userService.userData;
        console.log(this.userData);
    });    
  }

  /*
  guilherme natal 22/02/2018
  A função logout faz o logout do usuário e volta para a página inícial
  */

  public Logout():void{
    this.BackToInitialPage();
  }
  public BackToInitialPage():void{
    location.reload();
    this.DeleteAllCookies();
}

/*
Guilherme Natal 23/02/2018
A função DeleteAllCookies deleta os cookies para que o usuário não
logue altomaticamente.
*/

  public DeleteAllCookies() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i];
      var eqPos = cookie.indexOf("=");
      var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
      document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
  }

}
