import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';

import { UserService } from '../../services/user.service';

@Component({
  selector: 'page-retrive-password',
  templateUrl: 'retrive-password.html',
})
export class RetrivePasswordPage {

  private email:string;
  private userObserver:any;

  constructor(  public navCtrl: NavController,
                public navParams: NavParams,
                public userService:UserService,
                public alertController:AlertController
              ) {
  }

  /*
  Guilherme Natal 01/03/2018
  a função SendMail envia um email com o link para recuperação de senha  
  */

  public SendMail():void{
    console.log(this.email);
    this.userObserver = this.userService
    .FindUserByEmail(this.email)
    .subscribe( (data:any) => {
      console.log(data['_body']);
      if(data['_body'] != ""){
        let alert = this.alertController.create({
          title:'email enviado',
          message: 'entre no email para recuperar sua senha',
          buttons:[{
            text:'ok'
            }
          ]
        });
        alert.present();
        setInterval(this.Send(), 2000);
      }
      else{
        let alert = this.alertController.create({
          title:'usuário não encontrado',
          message: 'inserir um usuário valido',
          buttons:[{
            text:'ok'
            }
          ]
        });
        alert.present();
      }
    });
  }

  public Send():void{
    this.userService.SendMail(this.email);
  }

}
