import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TecnologyDetail } from '../tecnology-detail/tecnology-detail';
import { Projects } from '../projects/projects';
import { Services } from '../services/services';
import { LoginPage } from '../login/login'

@IonicPage()
@Component({
  selector: 'page-tecnology-project',
  templateUrl: 'tecnology-project.html',
})
export class TecnologyProject {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }

  goTecnologyDetail():void
  {
    this.navCtrl.push(TecnologyDetail);
  }

  goProjects():void
  {
    this.navCtrl.push(Projects);
  }

  goServices():void
  {
    this.navCtrl.push(Services);
  }

  goTecnologyProject():void
  {
    this.navCtrl.push(TecnologyProject);
  }

  goToLogin():void
  {
    this.navCtrl.push(LoginPage);
  }
}
