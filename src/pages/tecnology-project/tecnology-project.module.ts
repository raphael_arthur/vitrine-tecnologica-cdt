import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TecnologyProject } from './tecnology-project';

@NgModule({
  declarations: [
    TecnologyProject,
  ],
  imports: [
    IonicPageModule.forChild(TecnologyProject),
  ],
  exports: [
    TecnologyProject
  ]
})
export class TecnologyProjectModule {}
