import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DepartmentService } from '../../services/department.service';

@IonicPage()
@Component({
  selector: 'page-department-crud',
  templateUrl: 'department-crud.html',
})
export class DepartmentCrudPage {

  private departments_observer:any;
  private departments:any;
  private newDep:any;


  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl:ViewController, 
    private departmentService:DepartmentService, 
    private formBuilder:FormBuilder) 
  {
    this.newDep =
    {
      'nome':  "",
      'sigla': ""
    };

    this.getDepartments();
  }

  public doRefresh(refresher) {
    this.getDepartments();
    this.navCtrl.push(this.navCtrl.getActive().component).then(() => {
      let index = this.viewCtrl.index;
      this.navCtrl.remove(index);
   })
  }

  private getDepartments():void
  {
    this.departments_observer = this.departmentService.getAllDepartments().subscribe( data => {
        this.departments = this.departmentService.department_found;
    });
  }

  private del(id):void
  {
    this.departmentService.del(id).subscribe( data => {
        this.departments = this.departmentService.department_found;
    });
    this.doRefresh(0);
  }
  
  private edit(department:any):void
  {
    this.departmentService.edit(department);
    this.doRefresh(0);
  }

  private create(department:any):void
  {
    this.departmentService.create(department);
    this.doRefresh(0);
  }

    /*
  
  Guilherme Natal 23/02/2018
  A função Logout desloga o usário do banco de dados
  e retorna a página inícial
  */

  public Logout():void{
    this.BackToInitialPage();
  }
  public BackToInitialPage():void{
    location.reload();
    this.DeleteAllCookies();
  }

/*
Guilherme Natal 23/02/2018
A função DeleteAllCookies deleta os cookies para que o usuário não
logue altomaticamente.
*/

  public DeleteAllCookies() {
      var cookies = document.cookie.split(";");

      for (var i = 0; i < cookies.length; i++) {
          var cookie = cookies[i];
          var eqPos = cookie.indexOf("=");
          var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
          document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
      }
  }

}
