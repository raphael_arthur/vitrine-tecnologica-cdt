import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DepartmentCrudPage } from './department-crud';

@NgModule({
  declarations: [
    DepartmentCrudPage,
  ],
  imports: [
    IonicPageModule.forChild(DepartmentCrudPage),
  ],
  exports: [
    DepartmentCrudPage
  ]
})
export class DepartmentCrudPageModule {}
