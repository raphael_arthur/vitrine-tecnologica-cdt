import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProjectCrudPage } from './project-crud';

@NgModule({
  declarations: [
    ProjectCrudPage,
  ],
  imports: [
    IonicPageModule.forChild(ProjectCrudPage),
  ],
  exports: [
    ProjectCrudPage
  ]
})
export class ProjectCrudPageModule {}
