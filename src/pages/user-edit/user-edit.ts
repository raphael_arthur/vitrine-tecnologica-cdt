import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserService } from '../../services/user.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-user-edit',
  templateUrl: 'user-edit.html',
})
export class UserEditPage {

  private userID:any;
  private userData:any;
  private user_observer:any;

  constructor(  public navCtrl: NavController,
                public navParams: NavParams,
                public userService: UserService,
                private formBuilder:FormBuilder,
                public loading: LoadingController,
                public alert:AlertController
              ) 
  {
    
    this.userID = sessionStorage.getItem('userID');
    this.userData = navParams.get('userData');
    this.userData.nome = "";
    this.userData.sobrenome = "";
    console.log(this.userData);
  }

  getUser(userID):any
  {
    this.user_observer = this.userService.getUser(userID).subscribe( data => {
        this.userData = this.userService.userData;
        console.log(this.userData);
    });
    
  }

  editForm():void{
    let validInput:boolean = false;
    if(this.userData.newPassword == this.userData.newPasswordConfirm){
      validInput = true;
    }

    if(validInput){
      this.userService.editUser(this.userData).subscribe( data => {
          console.log("dentro do subscribe");
      });
    }
    else{
      let popUp = this.alert.create({
        title:"senhas diferentes",
        subTitle:"informe senhas iguais",
        buttons:["ok"]
      });
      popUp.present();
    }

  }
    /*
  
  Guilherme Natal 23/02/2018
  A função Logout desloga o usário do banco de dados
  e retorna a página inícial
  */

  public Logout():void{
    this.BackToInitialPage();
  }
  public BackToInitialPage():void{
    location.reload();
    this.DeleteAllCookies();
  }

/*
Guilherme Natal 23/02/2018
A função DeleteAllCookies deleta os cookies para que o usuário não
logue altomaticamente.
*/

  public DeleteAllCookies() {
      var cookies = document.cookie.split(";");

      for (var i = 0; i < cookies.length; i++) {
          var cookie = cookies[i];
          var eqPos = cookie.indexOf("=");
          var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
          document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
      }
  }

}
