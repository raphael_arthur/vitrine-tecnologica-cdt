import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServicesCrudPage } from './services-crud';

@NgModule({
  declarations: [
    ServicesCrudPage,
  ],
  imports: [
    IonicPageModule.forChild(ServicesCrudPage),
  ],
  exports: [
    ServicesCrudPage
  ]
})
export class ServicesCrudPageModule {}
