import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ServiceService } from '../../services/service.service';
import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';

//import { Service } from './interfaceService';
import { Service } from '../../models/services';

@IonicPage()
@Component({
  selector: 'page-services-crud',
  templateUrl: 'services-crud.html',
})

export class ServicesCrudPage {
  
  public formService: FormGroup;
  private service_observer:any;
  private services:any;
  private newService:Array<Service>;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController,
    private serviceService:ServiceService,
    private formBuilder:FormBuilder,
  ) 
  {
    let temp = new Service();
    this.newService = [];
    this.newService.push(temp);
    
    console.log(this.newService);
    this.formService = this.formBuilder.group({
      services: this.formBuilder.array([
        this.initFields()
      ])
    });
    this.getServices();
  }

  public initFields():FormGroup{
    return this.formBuilder.group({
      name:['', Validators.required],
      subname:['', Validators.required],
    });
  }
  public teste(i:number){
    const control = <FormArray>this.formService.controls.services;
    control.setValue.arguments.subname = "lalala";
    return "lalalalalala"
  }

  public teste2(){
    console.log(this.newService);
  }

  public addNewSubField(k:number):void {
    let temp = "";
    this.newService[k].subname.push(temp);
  }

  public removeSubField(k:number, j:number):void{
    this.newService[k].subname.splice(j,1);
  }

  public addNewField():void{
    let temp = new Service();
    this.newService.push(temp);
  }

  public removeField(k:number):void{
    this.newService.splice(k,1);
  }

  public addNewInputField():void{
    const control = <FormArray>this.formService.controls.equips;
    control.push(this.initFields());
  }

  public removeInputField(i:number):void{
    const control = <FormArray>this.formService.controls.equips;
    control.removeAt(i);
  }

  public manage(val:any):void{
    console.dir(val); 
  }

  public doRefresh(refresher) {
    this.getServices();
    this.navCtrl.push(this.navCtrl.getActive().component).then(() => {
      let index = this.viewCtrl.index;
      this.navCtrl.remove(index);
   })
  }


  private getServices():void{
    this.service_observer = this.serviceService.getAllServices().subscribe( data => {
        this.services = this.serviceService.service_found;
    });
  }

  private del(id):void{
    this.serviceService.del(id).subscribe( data => {
        this.services = this.serviceService.service_found;
    });
    this.doRefresh(0);
  }
  
  private edit(service:any):void{
    this.serviceService.edit(service);
    this.doRefresh(0);
  }

  private create(service:any):void{    
    this.serviceService.create(service);
    this.doRefresh(0);
  }

}
