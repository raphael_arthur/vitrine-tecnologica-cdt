import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoryCrudPage } from './category-crud';

@NgModule({
  declarations: [
    CategoryCrudPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoryCrudPage),
  ],
  exports: [
    CategoryCrudPage
  ]
})
export class CategoryCrudPageModule {}
