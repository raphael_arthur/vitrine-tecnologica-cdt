import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { CategoryService } from '../../services/category.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-category-crud',
  templateUrl: 'category-crud.html',
})
export class CategoryCrudPage {

  private category_observer:any;
  private categories:any;
  private newCat:string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController,
    private categoryService:CategoryService,
    private formBuilder:FormBuilder
  ) 
  {
    this.newCat = "";
    this.getCategories();
  }

  public doRefresh(refresher) {
    this.getCategories();
    this.navCtrl.push(this.navCtrl.getActive().component).then(() => {
      let index = this.viewCtrl.index;
      this.navCtrl.remove(index);
   })
  }


  private getCategories():void
  {
    this.category_observer = this.categoryService.getAllCategories().subscribe( data => {
        this.categories = this.categoryService.category_found;
    });
  }

  private del(id):void{
    this.categoryService.del(id).subscribe( data => {
        this.categories = this.categoryService.category_found;
    });
    this.doRefresh(0);
  }
  
  private edit(category:any):void
  {
    this.categoryService.edit(category);
    this.doRefresh(0);
  }

  private create(department:any):void
  {
    console.log(this.newCat);
    this.categoryService.create(department);
    this.doRefresh(0);
  }

    /*
  
  Guilherme Natal 23/02/2018
  A função Logout desloga o usário do banco de dados
  e retorna a página inícial
  */

  public Logout():void{
    this.BackToInitialPage();
  }

  public BackToInitialPage():void{
    location.reload();
    this.DeleteAllCookies();
  }

/*
Guilherme Natal 23/02/2018
A função DeleteAllCookies deleta os cookies para que o usuário não
logue altomaticamente.
*/

  public DeleteAllCookies() {
      var cookies = document.cookie.split(";");

      for (var i = 0; i < cookies.length; i++) {
          var cookie = cookies[i];
          var eqPos = cookie.indexOf("=");
          var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
          document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
      }
  }

}
