import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeacherCrudPage } from './teacher-crud';

@NgModule({
  declarations: [
    TeacherCrudPage,
  ],
  imports: [
    IonicPageModule.forChild(TeacherCrudPage),
  ],
  exports: [
    TeacherCrudPage
  ]
})
export class TeacherCrudPageModule {}
