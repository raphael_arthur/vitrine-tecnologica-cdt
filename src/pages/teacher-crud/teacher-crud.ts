import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { InventorService } from '../../services/inventor.service';
import { FileUploadService } from '../../services/UploadFileService';

@IonicPage()
@Component({
  selector: 'page-teacher-crud',
  templateUrl: 'teacher-crud.html',
})
export class TeacherCrudPage {
 
  private teacher_observer:any;
  private teachers:any;
  private newTeacher:any;

  public fileToUpload: File = null;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private teacherService:InventorService,
    public fileUploadService:FileUploadService
  ) 
  {
    this.newTeacher = 
    {
      'nome':  "",
      'lattes': "",
      'fileName':""
    };
    this.getTeachers();
  }
  
  public doRefresh(refresher) {
    this.getTeachers();
    this.navCtrl.push(this.navCtrl.getActive().component).then(() => {
      let index = this.viewCtrl.index;
      this.navCtrl.remove(index);
   });
  }


  private getTeachers():void
  {
    this.teacher_observer = this.teacherService.getAllInventores().subscribe( data => {
        this.teachers = this.teacherService.inventor_found;
    });
  }

  private del(id):void{
    this.teacherService.del(id).subscribe( data => {
        this.teachers = this.teacherService.inventor_found;
    });
    this.doRefresh(0);
  }
  

  public HandleFileInput(files: FileList) {
    this.fileToUpload = document.forms['formImage']['file'].files[0];
  }

  private edit(teacher:any):void
  {
    this.teacherService.edit(teacher);
    this.doRefresh(0);
  }

  private create(teacher:any):void
  {
    let body = new FormData();
    let hasFile:boolean = false;
    if(this.fileToUpload){
      hasFile = true;
      body.append('file',this.fileToUpload,this.fileToUpload.name);
    }
    if(hasFile){
      teacher.fileName = this.fileToUpload.name;
      this.fileUploadService.PostFile(body,'http://localhost:8080/PhpMess/upload_file.php').subscribe(data => {
        console.log(data);
      }, error => {
        console.log(error);
      });
    }
    this.teacherService.create(teacher);
    this.doRefresh(0);
  }

  /*  
  Guilherme Natal 23/02/2018
  A função Logout desloga o usário do banco de dados
  e retorna a página inícial
  */

  public Logout():void{
    this.BackToInitialPage();
  }

  public BackToInitialPage():void{
    location.reload();
    this.DeleteAllCookies();
  }

  /*
  Guilherme Natal 23/02/2018
  A função DeleteAllCookies deleta os cookies para que o usuário não
  logue altomaticamente.
  */

  public DeleteAllCookies() {
      var cookies = document.cookie.split(";");

      for (var i = 0; i < cookies.length; i++) {
          var cookie = cookies[i];
          var eqPos = cookie.indexOf("=");
          var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
          document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
      }
  }


}
