import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { EquipmentService } from '../../services/equipment.service';
import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';

import { FileUploadService } from '../../services/UploadFileService';


@IonicPage()
@Component({
  selector: 'page-equipments',
  templateUrl: 'equipments.html',
})
export class EquipmentsPage {

  public formEquip: FormGroup;
  //public equips:any = [];
  private equipment_observer:any;
  private equipments:any;
  private newEquip:any;
  public fileToUpload:File = null;



  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController,
    private equipmentService:EquipmentService,
    private formBuilder:FormBuilder,
    private fileUploadService:FileUploadService
  ) 
  {
    this.newEquip = {
      'name':"",
      'fileName':""
    };
    this.formEquip = this.formBuilder.group({
      equips: this.formBuilder.array([
        this.initEquipFields()
      ])
    });
    
    this.getEquipments();
  }


  /*
  Guilherme Natal 13/03/2018
  A função HandleFileInput pega a o arquivo escolhido do formulario
  e coloca em fileToUpload
  */

  public HandleFileInput(files: FileList) {
    this.fileToUpload = document.forms['formImage']['file'].files[0];
  }

  public doRefresh(refresher) {
    this.getEquipments();
    this.navCtrl.push(this.navCtrl.getActive().component).then(() => {
      let index = this.viewCtrl.index;
      this.navCtrl.remove(index);
   })
  }

  public initEquipFields():FormGroup{
    return this.formBuilder.group({
      name:['', Validators.required],
    });
  }

  public addNewInputField():void{
    const control = <FormArray>this.formEquip.controls.equips;
    control.push(this.initEquipFields());
  }

  public removeInputField(i:number):void{
    const control = <FormArray>this.formEquip.controls.equips;
    control.removeAt(i);
    
  }

  public manage(val:any):void{
    console.dir(val); 
  }

  private getEquipments():void{
    this.equipment_observer = this.equipmentService.getAllEquipments().subscribe( data => {
        this.equipments = this.equipmentService.equipment_found;
    });
  }

  private del(id):void{
    this.equipmentService.del(id).subscribe( data => {
        this.equipments = this.equipmentService.equipment_found;
    });
    this.doRefresh(0);
  }
  
  private edit(category:any):void{
    this.equipmentService.edit(category);
    this.doRefresh(0);
  }

  private create(equip:any):void{
    let body = new FormData();
    let hasFile:boolean = false;
    if(this.fileToUpload){
      hasFile = true;
      body.append('file',this.fileToUpload,this.fileToUpload.name);
    }
    if(hasFile){
      equip.fileName = this.fileToUpload.name;
      this.fileUploadService.PostFile(body,'http://localhost:8080/server_vitrine/request/upload_file.php')
      .subscribe(data => {
        console.log(data);
      }, error => {
        console.log(error);
      });  
    }


    this.equipmentService.create(equip);
    this.doRefresh(0);
  }

}
