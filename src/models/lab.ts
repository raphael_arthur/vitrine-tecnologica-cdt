import { Service } from './services';

export class Lab{
    public name:string;
    public desc:string;
    public sigla:string;
    public coord_nome:string;
    public coord_lattes:string;
    public idinventor:string;
    public iddepartamento:string;
    public idpalavra_chave:string;
    public telefone:string;
    public contato:string;
    public email:string;
    public endereco:string;
    public cidade:string;
    public UF:string;
    public CEP:string;
    public servico:Array<Service>
    constructor()
    {
        let temp = new Service();
        this.servico.push(temp);
    }
}