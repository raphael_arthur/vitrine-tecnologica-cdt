import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';

import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { FileChooser } from '@ionic-native/file-chooser';
//------------------Pages-------------------------------------------
import { MyApp } from './app.component';                            
import { HomePage } from '../pages/home/home';                      
//import { Projects } from '../pages/projects/projects';              
import { Services } from '../pages/services/services';              
//import { TecnologyProject } from '../pages/tecnology-project/tecnology-project';
//import { TecnologyDetail } from '../pages/tecnology-detail/tecnology-detail';
//import { Labs } from '../pages/labs/labs';  
import { LoginPage } from '../pages/login/login';                        
import { SignUpPage } from '../pages/signup/signup';
//import { EditPage } from '../pages/edit/edit';
//import { UserEditPage } from '../pages/user-edit/user-edit';
//import { DepartmentCrudPage } from '../pages/department-crud/department-crud';
//import { CategoryCrudPage } from '../pages/category-crud/category-crud';
//import { KeyWordCrudPage } from '../pages/key-word-crud/key-word-crud';
//import { SectorCrudPage } from '../pages/sector-crud/sector-crud';
//import { TeacherCrudPage } from '../pages/teacher-crud/teacher-crud';
//import { TecnologyCrudPage } from '../pages/tecnology-crud/tecnology-crud';
import { ModalCreateTech } from '../pages/tecnology-crud/modal-create';
import { ModalEditTech } from '../pages/tecnology-crud/modal-edit';
//import { LabCrudPage } from '../pages/lab-crud/lab-crud';
import { ModalCreateLab } from '../pages/lab-crud/modal-create';
import { ModalEditLab } from '../pages/lab-crud/modal-edit';
import { ModalCreateKeyWord } from  '../pages/key-word-crud/modal-create'
//import { TecnologyDescPage } from '../pages/tecnology-desc/tecnology-desc';
//----------------------PLUGINS--------------------------------------
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { RetrivePasswordPage } from '../pages/retrive-password/retrive-password';

//---------------------PROVIDERS--------------------------------

import { Data } from '../providers/data';
import { Service } from '../models/services';
//----------------------CORE MODULE-------------------------------
import { CoreModule } from './core/core.module';

//-----------------------SERVICES---------------------------------
import { UserService } from '../services/user.service';
import { DepartmentService } from '../services/department.service';
import { SetorService } from '../services/setor.service';
import { CategoryService } from '../services/category.service';
import { KeyService } from '../services/key.service';
import { InventorService } from '../services/inventor.service';
import { TechService } from '../services/tecnology.service';
import { LabService } from '../services/laboratory.service'
import { EquipmentService } from '../services/equipment.service';
import { ServiceService } from '../services/service.service';
import {FileUploadService} from '../services/UploadFileService'
//----------------------------------------------------------------

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    //Projects,
    Services,
    //TecnologyProject,
    //TecnologyDetail,
    //Labs,
    LoginPage,
    SignUpPage,
    //EditPage,
    //UserEditPage,
    //DepartmentCrudPage,
    //CategoryCrudPage,
    //KeyWordCrudPage,
    //SectorCrudPage,
    //TeacherCrudPage,
    //TecnologyCrudPage,
    RetrivePasswordPage,
    ModalCreateTech,
    ModalEditTech,
    //LabCrudPage,
    ModalCreateLab,
    ModalEditLab,
    ModalCreateKeyWord,
    //TecnologyDescPage,

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    CoreModule,
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    //Projects,
    Services,
    //TecnologyProject,
    //TecnologyDetail,
    //Labs,
    LoginPage,
    SignUpPage,
    //EditPage,
    //UserEditPage,
    //DepartmentCrudPage,
    //CategoryCrudPage,
    //KeyWordCrudPage,
    /*SectorCrudPage,
    TeacherCrudPage,
    TecnologyCrudPage,*/
    RetrivePasswordPage,
    ModalCreateTech,
    ModalEditTech,
    ModalCreateKeyWord,
    //LabCrudPage,
    ModalCreateLab,
    ModalEditLab,
    //TecnologyDescPage,
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Data,
    UserService,
    DepartmentService,
    SetorService,
    CategoryService,
    KeyService,
    InventorService,
    TechService,
    LabService,
    EquipmentService,
    ServiceService,
    FileTransfer,
    FileUploadService,
    FileTransferObject,
    FileChooser,
    File
  ]
})
export class AppModule {}
