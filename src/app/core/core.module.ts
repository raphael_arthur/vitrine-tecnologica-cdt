import { NgModule, Optional, SkipSelf } from '@angular/core';

import { AuthService } from './services/auth.service';
import { CategoryCrudPageModule } from '../../pages/category-crud/category-crud.module';
import { DepartmentCrudPageModule } from '../../pages/department-crud/department-crud.module';
import { EditPageModule } from '../../pages/edit/edit.module';
import { KeyWordCrudPageModule } from '../../pages/key-word-crud/key-word-crud.module';
import { LabCrudPageModule } from '../../pages/lab-crud/lab-crud.module';

import { ProjectsModule } from '../../pages/projects/projects.module';
import { SectorCrudPageModule } from '../../pages/sector-crud/sector-crud.module';
import { ServicesModule } from '../../pages/services/services.module';
import { TeacherCrudPageModule } from '../../pages/teacher-crud/teacher-crud.module';
import { TecnologyCrudPageModule } from '../../pages/tecnology-crud/tecnology-crud.module';
import { TecnologyDescPageModule } from '../../pages/tecnology-desc/tecnology-desc.module';
import { TecnologyDetailModule } from '../../pages/tecnology-detail/tecnology-detail.module';
import { TecnologyProjectModule } from '../../pages/tecnology-project/tecnology-project.module';
import { UserEditPageModule } from '../../pages/user-edit/user-edit.module';
import { EquipmentsPageModule } from '../../pages/equipments/equipments.module';
import { ServicesCrudPageModule } from '../../pages/services-crud/services-crud.module';
import { ServiceSearchPageModule } from '../../pages/service-search/service-search.module';
import { ServiceDescPageModule } from '../../pages/service-desc/service-desc.module';

/**
 * This core module exports and provide service instance for the aplication level
 * we import it once in the AppModule when the app starts and never import anywhere else.
 * Here we call services and components needed aplication wide (with only and only one instance)
 *
 * @export
 * @class CoreModule
 */

@NgModule({
    //declarations: [ ],

    imports: [  CategoryCrudPageModule,
                DepartmentCrudPageModule,
                EditPageModule,
                KeyWordCrudPageModule,
                LabCrudPageModule,
                
                ProjectsModule,
                SectorCrudPageModule,
                TeacherCrudPageModule,
                TecnologyCrudPageModule,
                TecnologyDescPageModule,
                TecnologyDetailModule,
                TecnologyProjectModule,
                UserEditPageModule,
                EquipmentsPageModule,
                ServicesCrudPageModule,
                ServiceSearchPageModule,
                ServiceDescPageModule,
                
                
    ],

    //exports: [ ],

    providers: [ AuthService ]
})
export class CoreModule {


    /**
     * Creates an instance of CoreModule.
     * @param {CoreModule} parentModule
     *
     * SOME EXPLANATION IS NEED (FROM ANGULAR DOCUMENTATION)
     *
     * The constructor tells Angular to inject the CoreModule into itself. That seems dangerously circular.
     * The injection would be circular if Angular looked for CoreModule in the current injector.
     * The @SkipSelf decorator means "look for CoreModule in an ancestor injector, above me in the injector hierarchy."
     * If the constructor executes as intended in the AppModule, there is no ancestor injector that could provide an instance of CoreModule.
     * The injector should give up.
     * By default the injector throws an error when it can't find a requested provider.
     * The @Optional decorator means not finding the service is OK.
     * The injector returns null, the parentModule parameter is null, and the constructor concludes uneventfully.
     *
     * @memberOf CoreModule
     */
    constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
        console.log("Core Module Instance Created");
        if (parentModule) {
            throw new Error(
            'CoreModule is already loaded. Import it in the AppModule only');
        }
    }

}
